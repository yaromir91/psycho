var keystone = require('keystone'),
	_ = require('underscore'),
	CoursesInfo = keystone.list('CoursesInfo'),
	Testimonials = keystone.list('Testimonials');


exports = module.exports = function (req, res) {

	var result = {},
		courseItem = {};
	CoursesInfo.paginate({
			page: req.query.page || 1,
			perPage: 10,
			maxPages: 10
		})
		.select('-_id -__v')
		.exec(function (err, ci) {

			if(err) res.status(422).json(err);
			
			if(!ci.results.length) res.json(result);
			
			_.each(ci.results, function (item) {
				courseItem.email = item.email;
				courseItem.phone = item.phone;
				courseItem.videoUrl = item.videoUrl;
				courseItem.titleTwo = item.titleTwo;
				courseItem.descriptionOne = item.descriptionOne;
				courseItem.titleOne = item.titleOne;
				courseItem.title = item.title;
				courseItem.bullets = item.bullets;
				
				Testimonials.model
					.find()
					.where('_id')
					.in(item.testimonials)
					.select('-_id title score description phone email')
					.exec(function (err, testimonials) {
						if(err) res.status(422).json(err);
						
						courseItem.testimonials = testimonials;
						result = courseItem;
						
						res.json(result);
					});

			});


		});
};
