$(document).ready(function() {

    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        navText: false,
        responsive: {
            0: {
                items: 1
            },
        }
    })

})