// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
require('dotenv').load();

// Require keystone
var keystone = require('keystone');
var swig = require('swig');
var body = require('body-parser');

// Disable swig's bulit-in template caching, express handles it
swig.setDefaults({
	cache: false
});

// Initialise Keystone with your project's configuration.
// See http://keystonejs.com/guide/config for available options
// and documentation.

keystone.init({

	'name': 'PsychoWeb',
	'brand': 'PsychoWeb',

	'sass': 'public/styles',

	'static': 'public',
	'favicon': 'public/favicon.ico',
	'views': 'templates/views',
	'view engine': 'swig',

	'custom engine': swig.renderFile,

	'auto update': true,
	'session': true,
	'auth': true,
	'user model': 'User',

	'cookie secret': 'FDS#@$@#FDSVSFS#@dfsfdsj3242dsFDSFDS',

	'wysiwyg additional options': {
		'force_br_newlines': true,
		'force_p_newlines': false,
		'forced_root_block': '',
		'directionality': 'rtl'
	},


	'port': '3000',
});

// Load your project's Models
keystone.import('models');

// Setup common locals for your templates. The following are required for the
// bundled templates and layouts. Any runtime locals (that should be set uniquely
// for each request) should be added to ./routes/middleware.js

keystone.set('locals', {
	_: require('underscore'),
	env: keystone.get('env'),
	utils: keystone.utils,
	editable: keystone.content.editable
});

// Settings for API request
keystone.app.use(body.json());

// Load your project's Routes

keystone.set('routes', require('./routes'));

// Configure the navigation bar in Keystone's Admin UI

keystone.set('nav', {
	'api': ['Courses', 'CoursesTypes', 'CoursesInfo', 'Testimonials'],
	'Navigation': 'Navigation',
	'Times': 'Times',
	'enquiries': ['enquiries', 'EnquiryPage'],
	'users': 'users',
	'shop': ['products', 'Card', 'ProductSchema'],
	'StaticPage': 'StaticPage',
	'sourcePage': ['SourcesPage', 'sliders'],
	'frontMain': ['HomePage', 'sliders', 'UsersSucceeded'],
	'coursesPage': 'CoursesPage',
	'wordsPage': 'WordsPage',
	'tables': ['Tables', 'TableOptions', 'TableColumns']
});

// Start Keystone to connect to your database and initialise the web server

keystone.start();
