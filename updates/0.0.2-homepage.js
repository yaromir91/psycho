var keystone = require('keystone'),
	_ = require('underscore'),
	async = require('async'),
	HomePageModel = keystone.list('HomePage'),
	Tables = keystone.list('Tables'),
	TableOptions = keystone.list('TableOptions'),
	TableColumns = keystone.list('TableColumns')
	;


var table = function (name) {
	return {
		"titleName" : name,
		"tableOptions": [],
		"tableColumns": []
	}
};


function createTables(done) {
	var tempTable,
		res = [table('home page circle 1'), table('home page circle 2'), table('home page circle 3'), table('home page circle 4')];
	async.series([
			function (next) {
				TableOptions.model.find().exec(function (err, data) {
					_.each(res, function (item) {
						item.tableOptions = _.pluck(data, '_id');
					});
					
					TableColumns.model.find().exec(function (err, data) {
						_.each(res, function (item) {
							item.tableColumns = _.pluck(data, '_id');
						});
						tempTable = res;

						next();
					});
				});
			},
			function (next) {
				
				_.each(tempTable, function (item, iter) {
					var newTable = new Tables.model(item);
					newTable.save(function (err, data) {
						if (err) {
							console.error("Error  to the database:");
							console.error(err);
						} else {
							console.log('Added table for home page.');
						}
					});
				});
				next();
			},
			function (next) {
				next()
			}
		],
		function () {
			done()
		})

}
	
function updateBlock2tableForCircle(done){
	HomePageModel.model.findOne().exec(function (err, homepage) {
		homepage.block9.mainTitle = '| מה עדיף - ללמוד לבד או בקורס?';
		homepage.block9.description = '<div class="blue-line"></div> ' +
			'<div class="sub-title"> נתחיל בהמלצה של המרכז הארצי לבחינות והערכה: </div> ' +
			'<div class="strong-text"> <b> ״המסגרת המתאימה להכנה היא עיניין של סגנון אישי: יש הזקוקים למסגרת נוקשה ויש המעדיפים לימוד עצמי. מכל מקום, לפני שתבחרו במסגרת' +
			' כלשהי, רצוי שתבדקו היטב אם היא מספקת לכם תירגול יסודי. אל תסתמכו על שמועות לא מבוססות בדבר ' +'שיפורים עצומים שהשיגו תלמידי קורס כלשהו בציוניהם וגם לא על נוסחאות קסם - אין כאלה!״</b> </div> ' +
			'<div class="text"> כן - אין תשובה חד משמעית האם עדיף ללמוד לבד לקראת מבחן פסיכומרי בעזרת קורס פסיכומטרי או בלימוד עצמי, זה באמת עיניין אישי. ככלל,' +
			' ככל שתלמיד יותר חזק עדיף לו ללמוד לבד. <span class="color-blue"><b>תלמיד חזק שילמד לבד יקבל ציון גבוה יותר מאשר אם ילמד בקורס!</b></span> נכו' +
			'ן, זה נשמע קצת מוזר, אבל הסיבה לכך היא שבקורס הזמן שלו מתבזבז על שאלות של תלמידים אחרים במקום להתמקד בצרכים שלו. </div>';
		homepage.set('block9.textHtml', undefined, {strict: false}); 
		homepage.save();
		done();
	});
}

exports = module.exports = function(done) {
	async.series([
			createTables,
			updateBlock2tableForCircle
		],
		function (e, res) {
			done()
		}

	);};
