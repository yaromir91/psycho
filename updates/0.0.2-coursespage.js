var keystone = require('keystone'),
	async = require('async'),
	_ = require('underscore'),
	CoursesPageModel = keystone.list('CoursesPage'),
	Tables = keystone.list('Tables'),
	TableOptions = keystone.list('TableOptions'),
	TableColumns = keystone.list('TableColumns')
;


var table = function (name) {
	return {
		"titleName" : name,
		"tableOptions": [],
		"tableColumns": []
	}
};


function createTables(done) {
	var tempTable,
		res = [table('courses page circle 1'), table('courses page circle 2'), table('courses page circle 3'), table('courses page circle 4')];
	async.series([
			function (next) {
				TableOptions.model.find().exec(function (err, data) {
					_.each(res, function (item) {
						item.tableOptions = _.pluck(data, '_id');
					});

					TableColumns.model.find().exec(function (err, data) {
						_.each(res, function (item) {
							item.tableColumns = _.pluck(data, '_id');
						});
						tempTable = res;

						next();
					});
				});
			},
			function (next) {

				_.each(tempTable, function (item, iter) {
					var newTable = new Tables.model(item);
					newTable.save(function (err, data) {
						if (err) {
							console.error("Error  to the database:");
							console.error(err);
						} else {
							console.log('Added table in source page.');
						}
					});
				});
				next();
			}
		],
		function () {
			done()
		})

}

var coursesPage = {
	block1 : {
		mainTitle : '<b>פסיכו</b> - ההכנה הטובה ביותר לפסיכומטרי',
		description : 'ךלש בצקב ,דומצ ךירדמ םע דומלל אוב'
	},
	block2 : {
		mainTitle : '?ונתיא אקווד דומלל המל',
		circleTitle1 : 'םירחתמה לומ ונחנאהאוושהל ץחל',
		tableForCircle1 : '',
		circleTitle2 : '...ףסכ ףסכםילקש יפלא ךוסח',
		tableForCircle2 : '',
		circleTitle3 : 'ךלש בצקב דומלל דומצ ךירדמ םע',
		tableForCircle3 : '',
		circleTitle4 : 'םינויצ עצוממ ץראב הובג יכה',
		tableForCircle4 : ''
	},
	block3 : {
		mainTitle : 'מקצוענים ממליצים',
		slides : []
	},
	block4 : {
		mainTitle : 'הצטרפו לאלפים שכבר הצליחו',
		UsersSucceeded : []
	},
	block5 : {
		mainTitle : '?דבוע הז ךיא',
		button : 'לצפייה בסרטון',
		circle1 : {
			mainTitle : 'המשרהב סרוקל',
			subTitle1 : 'חבמ ',
			subTitle2 : 'חבמ ',
			subTitle3 : 'ןחבמ ',
			subTitle4 : 'ןחבמ '
		},
		circle2 : {
			mainTitle : 'המשרהב סרוקל',
			subTitle1 : 'חבמ ',
			subTitle2 : 'חבמ ',
			subTitle3 : 'חבמ ',
			subTitle4 : 'חבמ '
		},
		circle3 : {
			mainTitle : 'חבמ ',
			subTitle1 : 'חבמ ',
			subTitle2 : 'חבמ ',
			subTitle3 : 'חבמ ',
			subTitle4 : 'חבמ '
		}
	},
	block6 : {
		mainTitle : 'תוצופנ תולאש',
		video : '',
		rightBlockTitle : ['?רתאב שי דומיל ירזע ולא', '?ירטמוכיספ ןויצ רפשל ךיא', '?ןחבמל םימשרנ ךיא' , '?הלב הלב הלב', '!?ויקוניפ'],
		rightBlockDescription : [
			'רתאב שי דומיל ירזע ולא רתאב שי דומיל ירזע ולא רתאב שי דומיל ירזע ולא רתאב שי דומיל ירזע ולא רתאב שי דומיל ירזע ולא רתאב שי דומיל ירזע ולא',
			'רתאב שי דומיל ירזע ולא רתאב שי דומיל ירזע ולא רתאב שי דומיל ירזע ולא רתאב שי דומיל ירזע ולא רתאב שי דומיל ירזע ולא רתאב שי דומיל ירזע ולא',
			'רתאב שי דומיל ירזע ולא רתאב שי דומיל ירזע ולא רתאב שי דומיל ירזע ולא רתאב שי דומיל ירזע ולא רתאב שי דומיל ירזע ולא רתאב שי דומיל ירזע ולא',
			'רתאב שי דומיל ירזע ולא רתאב שי דומיל ירזע ולא רתאב שי דומיל ירזע ולא רתאב שי דומיל ירזע ולא רתאב שי דומיל ירזע ולא רתאב שי דומיל ירזע ולא'
		]
	}
};

function setValueCoursesPageModel(done){
	setTimeout(function () {
		Tables.model.find({}, '_id').where('titleName').regex(/courses page circle /).exec(function (err, data) {
			_.each(data, function (item, key) {
				coursesPage.block2['tableForCircle' + (key+1)] = item._id;
			});
			done();
		});
	}, 500);
};

function coursesPageModel(done) {
	var newCoursesPage = new CoursesPageModel.model(coursesPage);

	newCoursesPage.save(function(err) {
		if (err) {
			console.error("Error  to the database:");
			console.error(err);
		} else {
			console.log("Added to the database.");
		}
		done(err);
	});
};

exports = module.exports = function(done) {
	async.series([
			createTables,
			setValueCoursesPageModel,
			coursesPageModel
		],
		function () {
			done()
		}

	);
};
