var keystone = require('keystone'),
		EnquiryPage = keystone.list('EnquiryPage');
var _ = require('underscore');
var async = require('async');

exports = module.exports = function(req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	locals.page = {};
	locals.form = {};
	locals.slides = [];

	view.on('init', function (toRender) {


		keystone
			.list('SourcesPage')
			.model
			.findOne()
			.populate('block4.slides', '-_id avatar title description')
			.exec(function(err, results) {
				// all result
				locals.page = results;
				locals.slides = results.block4.slides;
				toRender(err);

			});

			EnquiryPage.model.findOne().exec(function(err, form) {
				locals.form = form;
			});
	});

	view.render('sources');

};
