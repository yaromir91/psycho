var keystone = require('keystone'),
	Users = keystone.list('UsersSucceeded');

exports = module.exports = function(req, res) {
		var view = new keystone.View(req, res);
		var locals = res.locals;

		
		locals.teachers = {
			data : []
		};
		
		view.on('init', function (toRender) {	
			Users.model.find().where('_id').nin(req.body.teachers).exec(function(err, users) {
				locals.teachers.data = users;
				toRender();
			});
		});
	
		view.render('../partials/items-teachers');
};
