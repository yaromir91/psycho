var keystone = require('keystone');
var Types = keystone.Field.Types;
var _ = require('underscore');

/**
 * Sources Page Model
 * ==========
 */

var SourcesPage = new keystone.List('SourcesPage', {
	nocreate: true,
	nodelete : true
}),
	RelationshipName = 'Tables';

SourcesPage.add({
	block1 : {
		mainTitle: {type: Types.Html, wysiwyg: true, initial: true, width: 400, required: true},
		description: {type: Types.Html, wysiwyg: true, initial: true, width: 400, required: true}
	},
	block2 : {
		mainTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},

		circleTitle1 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		tableForCircle1 : { type: Types.Relationship,  ref: RelationshipName },
		textForCircle1: {type: Types.Html, wysiwyg: true, initial : true, width : 400, },
		videoForCircle1: {type: Types.Url, wysiwyg: true, initial : true, width : 400, },	
		circleSubtitle1 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},

		circleTitle2 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		tableForCircle2 : { type: Types.Relationship,  ref: RelationshipName },
		textForCircle2: {type: Types.Html, wysiwyg: true, initial : true, width : 400, },
		videoForCircle2: {type: Types.Url, wysiwyg: true, initial : true, width : 400, },
		circleSubtitle2 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},

		circleTitle3 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		tableForCircle3 : { type: Types.Relationship,  ref: RelationshipName },
		textForCircle3: {type: Types.Html, wysiwyg: true, initial : true, width : 400, },
		videoForCircle3: {type: Types.Url, wysiwyg: true, initial : true, width : 400, },
		circleSubtitle3 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		
		circleTitle4 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		tableForCircle4 : { type: Types.Relationship,  ref: RelationshipName },
		textForCircle4: {type: Types.Html, wysiwyg: true, initial : true, width : 400, },
		videoForCircle4: {type: Types.Url, wysiwyg: true, initial : true, width : 400, },
		circleSubtitle4 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true}
	},
	block3 : {
		mainTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		description : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		iconTitle : {
			'1' : {  
				type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true
			},
			'2' : {  
				type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true
			},
			'3' : {  
				type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true
			},
			'4' : {  
				type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true
			},
			'5' : {  
				type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true
			},
			'6' : {  
				type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true
			},
			'7' : {  
				type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true
			},
			'8' : {  
				type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true
			},
			'9' : {  
				type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true
			},
			'10' : {  
				type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true
			},
			'11' : {  
				type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true
			},
			'12' : {  
				type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true
			},
			'13' : {  
				type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true
			},
			'14' : {  
				type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true
			}
		},
		productForIcon1: {
			schema: {type: Types.Relationship, ref: 'ProductSchema',  initial : true},
			items: {type: Types.Relationship, ref: 'Product', many: true, initial : true}
		},
		productForIcon2: {
			schema: {type: Types.Relationship, ref: 'ProductSchema',  initial : true},
			items: {type: Types.Relationship, ref: 'Product', many: true, initial : true}
		},
		productForIcon3: {
			schema: {type: Types.Relationship, ref: 'ProductSchema',  initial : true},
			items: {type: Types.Relationship, ref: 'Product', many: true, initial : true}
		},
		productForIcon4: {
			schema: {type: Types.Relationship, ref: 'ProductSchema',  initial : true},
			items: {type: Types.Relationship, ref: 'Product', many: true, initial : true}
		},
		productForIcon5: {
			schema: {type: Types.Relationship, ref: 'ProductSchema',  initial : true},
			items: {type: Types.Relationship, ref: 'Product', many: true, initial : true}
		},
		productForIcon6: {
			schema: {type: Types.Relationship, ref: 'ProductSchema',  initial : true},
			items: {type: Types.Relationship, ref: 'Product', many: true, initial : true}
		},
		productForIcon7: {
			schema: {type: Types.Relationship, ref: 'ProductSchema',  initial : true},
			items: {type: Types.Relationship, ref: 'Product', many: true, initial : true}
		},
		productForIcon8: {
			schema: {type: Types.Relationship, ref: 'ProductSchema',  initial : true},
			items: {type: Types.Relationship, ref: 'Product', many: true, initial : true}
		},
		productForIcon9: {
			schema: {type: Types.Relationship, ref: 'ProductSchema',  initial : true},
			items: {type: Types.Relationship, ref: 'Product', many: true, initial : true}
		},
		productForIcon10: {
			schema: {type: Types.Relationship, ref: 'ProductSchema',  initial : true},
			items: {type: Types.Relationship, ref: 'Product', many: true, initial : true}
		},
		productForIcon11: {
			schema: {type: Types.Relationship, ref: 'ProductSchema',  initial : true},
			items: {type: Types.Relationship, ref: 'Product', many: true, initial : true}
		},
		productForIcon12: {
			schema: {type: Types.Relationship, ref: 'ProductSchema',  initial : true},
			items: {type: Types.Relationship, ref: 'Product', many: true, initial : true}
		},
		productForIcon13: {
			schema: {type: Types.Relationship, ref: 'ProductSchema',  initial : true},
			items: {type: Types.Relationship, ref: 'Product', many: true, initial : true}
		},
		productForIcon14: {
			schema: {type: Types.Relationship, ref: 'ProductSchema',  initial : true},
			items: {type: Types.Relationship, ref: 'Product', many: true, initial : true}
		}
	},
	block4 : {
		mainTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		slides : { type: Types.Relationship,  ref: 'Slider', many: true }
	}
		
});

SourcesPage.schema.pre('validate', function (next) {
	// block3.productForIcon1 todo
	if(_.isEmpty(this.block3.productForIcon1.schema) == _.isEmpty(this.block3.productForIcon1.items)){
		next(new Error('Use only one type productItems1 or productSchema1'));
	}if(_.isEmpty(this.block3.productForIcon2.schema) == _.isEmpty(this.block3.productForIcon2.items)){
		next(new Error('Use only one type productItems2 or productSchema2'));
	}if(_.isEmpty(this.block3.productForIcon3.schema) == _.isEmpty(this.block3.productForIcon3.items)){
		next(new Error('Use only one type productItems3 or productSchema3'));
	}if(_.isEmpty(this.block3.productForIcon4.schema) == _.isEmpty(this.block3.productForIcon4.items)){
		next(new Error('Use only one type productItems4 or productSchema4'));
	}if(_.isEmpty(this.block3.productForIcon5.schema) == _.isEmpty(this.block3.productForIcon5.items)){
		next(new Error('Use only one type productItems5 or productSchema5'));
	}if(_.isEmpty(this.block3.productForIcon6.schema) == _.isEmpty(this.block3.productForIcon6.items)){
		next(new Error('Use only one type productItems6 or productSchema6'));
	}if(_.isEmpty(this.block3.productForIcon7.schema) == _.isEmpty(this.block3.productForIcon7.items)){
		next(new Error('Use only one type productItems7 or productSchema7'));
	}if(_.isEmpty(this.block3.productForIcon8.schema) == _.isEmpty(this.block3.productForIcon8.items)){
		next(new Error('Use only one type productItems8 or productSchema8'));
	}if(_.isEmpty(this.block3.productForIcon9.schema) == _.isEmpty(this.block3.productForIcon9.items)){
		next(new Error('Use only one type productItems9 or productSchema9'));
	}if(_.isEmpty(this.block3.productForIcon10.schema) == _.isEmpty(this.block3.productForIcon10.items)){
		next(new Error('Use only one type productItems10 or productSchema10'));
	}if(_.isEmpty(this.block3.productForIcon11.schema) == _.isEmpty(this.block3.productForIcon11.items)){
		next(new Error('Use only one type productItems11 or productSchema11'));
	}if(_.isEmpty(this.block3.productForIcon12.schema) == _.isEmpty(this.block3.productForIcon12.items)){
		next(new Error('Use only one type productItems12 or productSchema12'));
	}if(_.isEmpty(this.block3.productForIcon13.schema) == _.isEmpty(this.block3.productForIcon13.items)){
		next(new Error('Use only one type productItems13 or productSchema13'));
	}if(_.isEmpty(this.block3.productForIcon14.schema) == _.isEmpty(this.block3.productForIcon14.items)){
		next(new Error('Use only one type productItems14 or productSchema14'));
	}

	// block2.tableForCircle todo
	if((_.isEmpty(this.block2.tableForCircle1)) == (_.isEmpty(this.block2.textForCircle1)) == (_.isEmpty(this.block2.videoForCircle1))){
		next(new Error('Use only one type for Block2: tableForCircle1 or tableForCircle1 or videoForCircle1'))
	}

	if((_.isEmpty(this.block2.tableForCircle2)) == (_.isEmpty(this.block2.textForCircle2)) == (_.isEmpty(this.block2.videoForCircle2))){
		next(new Error('Use only one type for Block2: tableForCircle1 or tableForCircle1 or videoForCircle1'))
	}

	if((_.isEmpty(this.block2.tableForCircle3)) == (_.isEmpty(this.block2.textForCircle3)) == (_.isEmpty(this.block2.videoForCircle3))){
		next(new Error('Use only one type for Block2: tableForCircle1 or tableForCircle1 or videoForCircle1'))
	}

	if((_.isEmpty(this.block2.tableForCircle4)) == (_.isEmpty(this.block2.textForCircle4)) == (_.isEmpty(this.block2.videoForCircle4))){
		next(new Error('Use only one type for Block2: tableForCircle1 or tableForCircle1 or videoForCircle1'))
	}
	next();
});

/**
 * Registration
 */

SourcesPage.register();
