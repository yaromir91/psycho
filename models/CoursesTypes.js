var keystone = require('keystone');
var Types = keystone.Field.Types;


/**
 * Sources Page Model
 * ==========
 */

var CoursesTypes = new keystone.List('CoursesTypes', {
	autokey: { path: 'slug', from: 'title', unique: true },
	map : { name: 'slug'}
});


CoursesTypes.add({
	slug : { type: String, readonly: true},
	title : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
	courses : { type: Types.Relationship, ref: 'Courses', many: true },
});



/**
 * Registration
 */

CoursesTypes.register();
