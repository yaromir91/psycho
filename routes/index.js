/**
 * This file is where you define your application routes and controllers.
 *
 * Start by including the middleware you want to run for every request;
 * you can attach middleware to the pre('routes') and pre('render') events.
 *
 * For simplicity, the default setup for route controllers is for each to be
 * in its own file, and we import all the files in the /routes/views directory.
 *
 * Each of these files is a route controller, and is responsible for all the
 * processing that needs to happen for the route (e.g. loading data, handling
 * form submissions, rendering the view template, etc).
 *
 * Bind each route pattern your application should respond to in the function
 * that is exported from this module, following the examples below.
 *
 * See the Express application routing documentation for more information:
 * http://expressjs.com/api.html#app.VERB
 */

var multer = require('multer');
var upload = multer({dest: './uploads/'});
var Excel = require('exceljs');
var fs = require('fs');
var models = require('../models-general');
var bodyParser = require('body-parser');
var config = require('../config');


var keystone = require('keystone');
var middleware = require('./middleware');
var importRoutes = keystone.importer(__dirname);

// Common Middleware
keystone.pre('routes', middleware.initErrorHandlers);
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

//init custom error
keystone.set('404', function(req, res) {
	res.notfound();
});
if(keystone.get('env') !== 'development'){
	keystone.set('500', function(err, req, res) {
		var title, message;
		if (err instanceof Error) {
			title = err.message;
			message = err.message;
			//err = err.stack;
		}
		res.err(err, title, message);
	});
}


// Import Route Controllers
var routes = {
	views: importRoutes('./views'),
	api: importRoutes('./api')
};

// Setup Route Bindings
exports = module.exports = function(app) {

	//API
	app.get('/api/courses-type', routes.api.coursesType);
	app.get('/api/courses-info', routes.api.coursesInfo);
	app.get('/api/shop', routes.api.apiShop);
	app.get('/api/times', routes.api.apiTimes);
	//console.log(routes.api.coursesType);

	// Views
	app.get('/', routes.views.index);
	app.post('/contact', routes.views.contact);

	app.get('/sources', routes.views.sources);
	app.get('/courses', routes.views.courses);

	// app.get('/products', routes.views.products);
	app.get('/card', routes.views.card);

	app.get('/order', routes.views.order);
	app.post('/order', routes.views.order);

	app.get('/products/:icon', routes.views.productSchema);
	app.get('/words', routes.views.words);

	//app.post('/uploadExcel', routes.views.uploadExcel);

	app.post('/uploadExcelHebrew', upload.single('avatar'), function (req, res, next) {
		var workbook = new Excel.Workbook();
		workbook.xlsx.readFile(req.files["excel"].path)
			.then(function() {
				workbook.eachSheet(function(worksheet, sheetId) {
					var columnsName = [];
					var firstRow = true;
					var row = worksheet.lastRow;
					var length = row.number;
					models.WordsInfo.updateDataWithCounter(config.LANGUAGES.hebrew,length,function (err,words){

					});
					worksheet.eachRow(function(row, rowNumber) {
						var currentRow = [];
						row.eachCell(function(cell, colNumber) {
							if(firstRow === true){
								columnsName.push({ header: cell.value, key: cell.value});
							}else{
							}
						});
						if(firstRow === true){
							firstRow = false;
							worksheet.columns = columnsName;
						}else{
							addHebrewWord(row);
						}
					});
					res.send("FINISHEd");
				});
			});
	});
	app.post('/uploadExcelEnglish', upload.single('avatar'), function (req, res, next) {
		console.log(req);
		var workbook = new Excel.Workbook();
		workbook.xlsx.readFile(req.files["excel"].path)
			.then(function() {
				workbook.eachSheet(function(worksheet, sheetId) {
					var columnsName = [];
					var firstRow = true;
					var row = worksheet.lastRow;
					var length = row.number;
					models.WordsInfo.updateDataWithCounter(config.LANGUAGES.english,length,function (err,words){
					});
					worksheet.eachRow(function(row, rowNumber) {
						var currentRow = [];
						row.eachCell(function(cell, colNumber) {
							if(firstRow === true){
								columnsName.push({ header: cell.value, key: cell.value});
							}else{
							}
						});
						if(firstRow === true){
							firstRow = false;
							worksheet.columns = columnsName;
						}else{
							addEnglishWord(row);
						}
					});
					res.send("FINISHEd");
				});
			});
	});



	app.get('/getWordsList', function (req, res, next) {
		console.log("getWordsList");
		if(!req.query.letters){
			res.send("Missing letters");
			return;
		}
		if(!req.query.language){
			res.send("Missing language");
			return;
		}
		if(!req.query.units){
			res.send("Missing units");
			return;
		}

		var language = req.query.language;

		if(language != config.LANGUAGES.english && language != config.LANGUAGES.hebrew){
			res.send("Invalid language");
			return;
		}

		var lettersArray = req.query.letters;
		var unitsArray = req.query.units;
		if(language == config.LANGUAGES.english){
			models.EnglishWord.getWordsList(lettersArray,unitsArray,function(err,words){
				var data = [];
				for(var i = 0 ; i<words.length ; i++){
					data.push(words[i].exportObject());//console.log(words[i].Word_First_Letter + " " + words[i].Unit);
				}
				res.send({words: data});
			});
		}else if(language == config.LANGUAGES.hebrew) {
			models.HebrewWord.getWordsList(lettersArray,unitsArray,function(err,words){
				var data = [];
				for(var i = 0 ; i<words.length ; i++){
					data.push(words[i].exportObject());//console.log(words[i].Word_First_Letter + " " + words[i].Unit);
				}
				res.send({words: data});
			});
		}else{
			res.send({words: []});
		}
	});

	app.get('/getWordsInfo', function (req, res, next) {
		models.WordsInfo.getWordsInfo(function (err, languages) {
			if(err) return next(err);

			var data = languages.map(function(language) {
				return language.exportObject();
			});
			res.send({
				languages: data
			});
		});
	});


	app.get('/:staticPage?', routes.views.staticPage);
	app.post('/table', routes.views.table);
	app.post('/teachers', routes.views.teachers);


	// NOTE: To protect a route so that only admins can see it, use the requireUser middleware:
	// app.get('/protected', middleware.requireUser, routes.views.protected);

};


function addHebrewWord(row){
	var ID = row.getCell('ID').value;
	var Unit = row.getCell('Unit').value;
	var Word_First_Letter = row.getCell('Word_First_Letter').value;
	var Word = row.getCell('Word').value;
	var Word_With_Nikud = row.getCell('Word_With_Nikud').value;
	var Meaning = row.getCell('Meaning').value;
	var Synonyms = row.getCell('Synonyms').value;
	var Sentence_1 = row.getCell('Sentence_1').value;
	models.HebrewWord.addNewWord(ID,Unit,Word_First_Letter,Word,Word_With_Nikud,Meaning,Synonyms,Sentence_1,function(err,result){
	});
}

function addEnglishWord(row){
	var ID_Word = row.getCell('ID_Word').value;
	var Word_First_Letter = row.getCell('Word_First_Letter').value;
	var Word = row.getCell('Word').value;
	var Meaning = row.getCell('Meaning').value;
	var Unit = row.getCell('Unit').value;
	var Sentence_1 = row.getCell('Sentence_1').value;
	var Conjunction_Type = row.getCell('Conjunction_Type').value;
	var Meaning_Arabic = row.getCell('Meaning_Arabic').value;
	var Conjunction_Type_Arabic = row.getCell('Conjunction_Type_Arabic').value;
	models.EnglishWord.addNewWord(ID_Word,Word_First_Letter,Word,Meaning,Unit,Sentence_1,Conjunction_Type,Meaning_Arabic,Conjunction_Type_Arabic,function(err,result){
	});
}
