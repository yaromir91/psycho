var keystone = require('keystone')
	Products = keystone.list('Product');

exports = module.exports = function(req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	locals.products = [];

	view.on('init', function (toRender) {

		Products.model.find().where('status', true).exec(function (err, products) {
			locals.products = products;
		});
		toRender();
	});

	view.render('products')
} 
