var keystone = require('keystone');
var Types = keystone.Field.Types;


/**
 * Sources Page Model
 * ==========
 */

var Testimonials = new keystone.List('Testimonials', {
	autokey: { path: 'slug', from: 'title', unique: true },
	map : { name: 'slug'}
});


Testimonials.add({
	slug : { type: String, readonly: true},
	title : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
	description : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
});



/**
 * Registration
 */

Testimonials.register();
