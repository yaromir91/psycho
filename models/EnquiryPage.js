var keystone = require('keystone');
var Types = keystone.Field.Types;
var _ = require('underscore');

var EnquiryPage = new keystone.List('EnquiryPage', {
  nocreate: true,
	nodelete : true,
  map : { name : 'name'}
});

EnquiryPage.add ({
    name: { type: String , required: true, index: true, initial: true},
    mainTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
    leftIconTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
    rightIconTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
    phone : { type: Types.Text, required : true, initial: true }
})

EnquiryPage.register();
