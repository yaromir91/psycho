var keystone = require('keystone'),
	async = require('async'),
	_ = require('underscore'),
	Order = keystone.list('Order');

exports = module.exports = function(req, res, next) {
	var view = new keystone.View(req, res),
		locals = res.locals;

	locals.productsFromCookie = req.cookies.order ? JSON.parse(req.cookies.order) : [];
	locals.products = [];
	locals.delivery = false;
	locals.priceOfAllProducts = 0;

	view.on('init', function(toRender) {
		//locals.products = locals.productsFromCookie;
		toRender();
	});
	

	view.on('post', function(toRender) {
		locals.products = locals.productsFromCookie;
		console.log(locals.productsFromCookie);	
	});
	
	view.render('order');
};
