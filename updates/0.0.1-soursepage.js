var keystone = require('keystone'),
	async = require('async'),
	_ = require('underscore'),
	SourcesPage = keystone.list('SourcesPage'),
	Tables = keystone.list('Tables'),
	TableOptions = keystone.list('TableOptions'),
	TableColumns = keystone.list('TableColumns'),
	Products = keystone.list('Product')
	;

function clearSchemas(next) {

	async.series([
		sourcePage,
		tablesForSourcePage,
		tableOptions,
		tableColumns
	], 
	function () {
		next();
	});
    
	function tableOptions(next) {
		TableOptions.model.find().remove(function (err, data) {
			console.log('Removed Options >>> ', data);
		});
		next()
	}
    
	function tableColumns(next) {
		TableColumns.model.find().remove(function (err, data) {
			console.log('Removed Columns >>> ', data);
		});
		next()
	}

	function tablesForSourcePage(next) {
		Tables.model.find().remove(function (err, data) {
			console.log('Removed Tables >>> ', data);
		});
		next()
	}
	
	function sourcePage(next) {
		SourcesPage.model.find().remove(function (err, data) {
			console.log('Removed SourcesPage >>> ', data);
		});
		next()
	}
}

var tableOptions = [{
			"titleName": "םירפס"
		}, {
			"titleName": "ימצע דומילל ז״ול"
		}, {
			"titleName": "תויצלומיס"
		}, {
			"titleName": "םירוביח תקידב"
		}, {
			"titleName": "םירועיש"
		}, {
			"titleName": "?ךלש ךירדמה ימ"
		}, {
			"titleName": "לוגרת תונכות"
		}, {
			"titleName": "ריחמ"
		}
];


var tableColumns = [
	{
		"titleName": "וכיספ",
		"optionValue": [
			"םירפס 20  תולאש 10,394\t",
			"םימי יפל תטרופמ\t",
			"6",
			"1-6",
			"ןיילנוא םירועיש",
			",ןויסינ תונש 15 - רציווש דעלא, םינחבמו םירפס רבחמ םיכירדמ רישכמ\t",
			"םינחבמ ,םילימ תנכות תויצלומיס חותינו6\t", "ח״ש 1,199-1,699\t"]
	}, {
		"titleName": "SelfClass ויק-ייה",
		"optionValue": [
			"םירפס 20  תולאש 10,394\t",
			"םימי יפל תטרופמ\t",
			"6",
			"1-6",
			"ןיילנוא םירועיש",
			",ןויסינ תונש 15 - רציווש דעלא, םינחבמו םירפס רבחמ םיכירדמ רישכמ\t",
			"םינחבמ ,םילימ תנכות תויצלומיס חותינו6\t", "ח״ש 1,199-1,699\t"]
	}, {
		"titleName": "Home םודיק",
		"optionValue": [
			"םירפס 20  תולאש 10,394\t",
			"םימי יפל תטרופמ\t",
			"6",
			"1-6",
			"ןיילנוא םירועיש",
			",ןויסינ תונש 15 - רציווש דעלא, םינחבמו םירפס רבחמ םיכירדמ רישכמ\t",
			"םינחבמ ,םילימ תנכות תויצלומיס חותינו6\t", "ח״ש 1,199-1,699\t"]
	}, {
		"titleName": "חוור בינ",
		"optionValue": [
			"םירפס 20  תולאש 10,394\t",
			"םימי יפל תטרופמ\t",
			"6",
			"1-6",
			"ןיילנוא םירועיש",
			",ןויסינ תונש 15 - רציווש דעלא, םינחבמו םירפס רבחמ םיכירדמ רישכמ\t",
			"םינחבמ ,םילימ תנכות תויצלומיס חותינו6\t", "ח״ש 1,199-1,699\t"]
	}
];

var table = {
	"titleName" : "source page migration",
	"tableOptions": [],
	"tableColumns": []
};

var sourcesPage = 
	{
		block1 : {
			mainTitle: 'ירטמוכיספל דבל דומלל ידכ ךירצש המ לכ',
			description: 'דועו תולאשל םורופ ,םירוביח תקידב ,לוגרת תנכות ,םירפס ,ןיילנוא םירועיש'
		},
		block2 : {
			mainTitle : '?ונתיא אקווד דומלל המל',
			circleTitle1 : 'ואידיוב רצק רבסה',
			circleSubtitle1 : 'הייפצל ץחל',
			tableForCircle1 : null,
			circleTitle2 : 'ונב ורחבש םינעוצקמה',
			circleSubtitle2 : 'המישרל ץחל',
			tableForCircle2 : null,
			circleTitle3 : 'םירפסב לוגרת 2 יפ',
			circleSubtitle3 : 'טוריפל ץחל',
			tableForCircle3 : null,
			circleTitle4 : 'םירחתמה לומ ונחנא',
			circleSubtitle4 : 'האוושהל ץחל',
			tableForCircle4 : null
		},
		block3 : {
			mainTitle : '?רתאב ונל שי המ',
			description : '...ןמזה תא וחק זא ,הברה שי',
			productForIcon1 : { items : []},
			productForIcon2 : { items : []},
			productForIcon3 : { items : []},
			productForIcon4 : { items : []},
			productForIcon5 : { items : []},
			productForIcon6 : { items : []},
			productForIcon7 : { items : []},
			productForIcon8 : { items : []},
			productForIcon9 : { items : []},
			productForIcon10 : { items : []},
			productForIcon11 : { items : []},
			productForIcon12 : { items : []},
			productForIcon13 : { items : []},
			productForIcon14 : { items : []},
			iconTitle : {
				1 : 'ןיילנוא םירועיש',
				2 : 'ןיילנוא םירועיש',
				3 : 'ירטמוכיספ ירפס',
				4 : 'ירטמוכיספ סרוק',
				5 : 'ירטמוכיספ סרוק',
				6 : 'ירטמוכיספ סרוק',
				7 : 'ירטמוכיספ סרוק',
				8 : 'ירטמוכיספ סרוק',
				9 : 'ירטמוכיספ סרוק',
				10 : 'ירטמוכיספ סרוק',
				11 : 'ירטמוכיספ סרוק',
				12 : 'ירטמוכיספ סרוק',
				13 : 'ירטמוכיספ סרוק',
				14 : 'ירטמוכיספ סרוק'
			}
		},
		block4 : {
			mainTitle : 'ןיילנוא םירועיש'
		}
};

function createTables(done) {
	
	async.series([
			function (next) {
				
				_.each(tableOptions, function (item) {
					var newTableOptions = new TableOptions.model(item);
					newTableOptions.save(function (err, data) {
						if (err) {
							console.error("Error  to the database:");
							console.error(err, '<<<<', item);
						} else {
							table.tableOptions.push(data._id);
						}
					});
				});
				next();
			},
			function (next) {
				
				_.each(tableColumns, function (item) {
					var newTableColumns = new TableColumns.model(item);
					newTableColumns.save(function (err, data) {
						if (err) {
							console.error("Error  to the database:");
							console.error(err);
						} else {
							table.tableColumns.push(data._id);
						}
					});
				});
				
				next();
			},
			function (next) {
				
				var products = {
					name: 'test products',
					rating : 4,
					prices : {
						total : 1000
					},
					createdAt : new Date(),
					status : true,
					description : '',
					option: '',
					phone: '12312312312312'
				};
				var product = new Products.model(products);
				product.save(function (err, product) {
					sourcesPage.block3.productForIcon1.items.push(product._id);
					sourcesPage.block3.productForIcon2.items.push(product._id);
					sourcesPage.block3.productForIcon3.items.push(product._id);
					sourcesPage.block3.productForIcon4.items.push(product._id);
					sourcesPage.block3.productForIcon5.items.push(product._id);
					sourcesPage.block3.productForIcon6.items.push(product._id);
					sourcesPage.block3.productForIcon7.items.push(product._id);
					sourcesPage.block3.productForIcon8.items.push(product._id);
					sourcesPage.block3.productForIcon9.items.push(product._id);
					sourcesPage.block3.productForIcon10.items.push(product._id);
					sourcesPage.block3.productForIcon11.items.push(product._id);
					sourcesPage.block3.productForIcon12.items.push(product._id);
					sourcesPage.block3.productForIcon13.items.push(product._id);
					sourcesPage.block3.productForIcon14.items.push(product._id);
					next();
				});
				
				
			},
			function (next) {
				var newTableOptions = new Tables.model(table);
				newTableOptions.save(function (err, data) {
					if (err) {
						console.error("Error  to the database:");
						console.error(err);
						next(err);
					} else {
						sourcesPage.block2.tableForCircle1 = data._id;
						sourcesPage.block2.tableForCircle2 = data._id;
						sourcesPage.block2.tableForCircle3 = data._id;
						sourcesPage.block2.tableForCircle4 = data._id;
						console.log('Added table in source page.');
						next();
					}
				});
			}
	],
	function () {
		done()
	})
}

function createSourcesPage(next){
	
	var newSourcesPage = new SourcesPage.model(sourcesPage);
	newSourcesPage.save(function(err) {
		if (err) {
			console.error("Error  to the database:");
			console.error(err);
			next(err);
		} else {
			console.log("Added to the database.");
			next();
		}
	});
}

exports = module.exports = function(done) {
	async.series([
			// clearSchemas,
			createTables,
			createSourcesPage
		],
		function (e, res) {
			done()
		}
		
	);
};
