var keystone = require('keystone');
var Types = keystone.Field.Types;
var _ = require('underscore');


/**
 * Sources Page Model
 * ==========
 */

var CoursesPage = new keystone.List('CoursesPage', {
	nocreate: true,
	nodelete : true
}),
	RelationshipName = 'Tables';


CoursesPage.add({
	block1 : {
		mainTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
		description : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true }
	},
	block2 : {
		mainTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		
		circleTitle1 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		tableForCircle1 : { type: Types.Relationship,  ref: RelationshipName },
		textForCircle1: {type: Types.Html, wysiwyg: true, initial : true, width : 400, },
		videoForCircle1: {type: Types.Url, wysiwyg: true, initial : true, width : 400, },

		circleTitle2 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		tableForCircle2 : { type: Types.Relationship,  ref: RelationshipName },
		textForCircle2: {type: Types.Html, wysiwyg: true, initial : true, width : 400, },
		videoForCircle2: {type: Types.Url, wysiwyg: true, initial : true, width : 400, },

		circleTitle3 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		tableForCircle3 : { type: Types.Relationship,  ref: RelationshipName },
		textForCircle3: {type: Types.Html, wysiwyg: true, initial : true, width : 400, },
		videoForCircle3: {type: Types.Url, wysiwyg: true, initial : true, width : 400, },

		circleTitle4 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		tableForCircle4 : { type: Types.Relationship,  ref: RelationshipName },
		textForCircle4: {type: Types.Html, wysiwyg: true, initial : true, width : 400, },
		videoForCircle4: {type: Types.Url, wysiwyg: true, initial : true, width : 400, },
	},
	block3 : {
		mainTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		slides : { type: Types.Relationship,  ref: 'Slider', many: true},
	},
	block4 : {
		mainTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		UsersSucceeded : { type: Types.Relationship,  ref: 'UsersSucceeded', many: true},
	},
	block5 : {
		mainTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		button : {  type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
		buttonUrl: { type: Types.Url, wysiwyg: true, initial : true, width : 400 },
		circle1 : { 
			mainTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
			subTitle1 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
			subTitle2 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
			subTitle3 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
			subTitle4 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true }
		},
		circle2 : { 
			mainTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
			subTitle1 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
			subTitle2 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
			subTitle3 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
			subTitle4 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true }
		},
		circle3 : { 
			mainTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
			subTitle1 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
			subTitle2 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
			subTitle3 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
			subTitle4 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true }
		}
	},
	block6 : {
		mainTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		video : { type: Types.Url, initial : true },
		rightBlockTitle : { type: Types.TextArray, default: [], initial: true, collapse: true },
		rightBlockDescription : { type: Types.TextArray, default: [], initial: true, collapse: true }
	}
	
});

CoursesPage.schema.pre('save', function(next) {

	if((_.isEmpty(this.block2.tableForCircle1)) == (_.isEmpty(this.block2.textForCircle1)) == (_.isEmpty(this.block2.videoForCircle1))){

		next(new Error('Use only one type for Block2: tableForCircle1 or tableForCircle1 or videoForCircle1'))
	}


	if((_.isEmpty(this.block2.tableForCircle2)) == (_.isEmpty(this.block2.textForCircle2)) == (_.isEmpty(this.block2.videoForCircle2))){

		next(new Error('Use only one type for Block2: tableForCircle1 or tableForCircle1 or videoForCircle1'))
	}

	if((_.isEmpty(this.block2.tableForCircle3)) == (_.isEmpty(this.block2.textForCircle3)) == (_.isEmpty(this.block2.videoForCircle3))){

		next(new Error('Use only one type for Block2: tableForCircle1 or tableForCircle1 or videoForCircle1'))
	}


	if((_.isEmpty(this.block2.tableForCircle4)) == (_.isEmpty(this.block2.textForCircle4)) == (_.isEmpty(this.block2.videoForCircle4))){

		next(new Error('Use only one type for Block2: tableForCircle1 or tableForCircle1 or videoForCircle1'))
	}
	next();
})

/**
 * Registration
 */

CoursesPage.register();
