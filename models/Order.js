var keystone = require('keystone');
var Types = keystone.Field.Types;
/**
 * Order Model
 * ==========
 */

var Order = new keystone.List('Order', {
	nocreate: true,
	noedit: true,
	map : { name : 'titleName'}
});

Order.add({
	titleName: { type: String , index: true},
	title: {type: Types.Text, required : true ,initial: true},
	price: {type: Types.Number, required : true ,initial: true},
	createdAt : { type: Date , default: Date.now}
});

/**
 * Registration
 */

Order.defaultColumns = 'titleName, createdAt';
Order.register();
