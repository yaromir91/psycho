var keystone = require('keystone');
var Types = keystone.Field.Types;
/**
 * Card Page Model
 * ==========
 */

var Card = new keystone.List('Card', {
	nocreate: true,
	nodelete : true,
	map : { name : '_id'}
});

Card.add({
	title: { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
	price: { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
	count: { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
	coutPrice: { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
	delivery: {
		free: {
			name: { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
			descriptions: { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true }
		},
		pay: {
			name: { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
			descriptions: { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
			status : { type : Types.Boolean, initial : true},
			cost: { type: Types.Text, wysiwyg: true, initial : true, width : 400, required: true }
		}
	},
	finalprice: { type: Types.Text, wysiwyg: true, initial : true, width : 400, required: true }
})

/**
 * Registration
 */

Card.defaultColumns = 'name, createdAt';
Card.register();
