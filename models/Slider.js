var keystone = require('keystone');
var Types = keystone.Field.Types;
var upload = {
	path: 'public/upload',
	prefix : '/upload/'
};

/**
 * Slider Model
 * =============
 */

var Slider = new keystone.List('Slider', {
	map : { name : 'slideName'}
});

Slider.uploadsDirectories = upload;


Slider.add({
	slideName : { type: String, required: true, initial : true},
	title: { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
	description: { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
	avatar : { 
		type: Types.LocalFile, 
		dest: upload.path,
		prefix : upload.prefix,
		label: 'Upload Avatar',
		format : function (item, file) {
			return '<img src="' + upload.prefix + file.filename +'" style="max-width: 50px;">';
		},
	},
	position: { type: Types.Number, initial : true, unique: true },
	createdAt: { type: Date, default: Date.now}
});



/**
 * Registration
 */

Slider.defaultColumns = 'slideName';
Slider.register();


exports.paths = upload;
