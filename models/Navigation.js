var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Slider Model
 * =============
 */

var Navigation = new keystone.List('Navigation', {
	map: { name: 'name'}
});

Navigation.add({
	name: { type: String },
	title: { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
	url: { type: Types.Url, required: true, index: true, initial: true },
	position: { type: Types.Number, initial : true, unique: true }
});

/**
 * Registration
 */

Navigation.defaultColumns = 'title, position';
Navigation.register();
