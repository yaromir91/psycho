var keystone = require('keystone');
var Types = keystone.Field.Types;
/**
 * Times Page Model
 * ==========
 */

var Times = new keystone.List('Times', {
    //nocreate: true,
    //nodelete : true,
    map : { name : 'name'}
});

Times.add({
    name: { type: String},
    block1: { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
    block2: { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
    block3: { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
});

/**
 * Registration
 */

Times.defaultColumns = 'name';
Times.register();
