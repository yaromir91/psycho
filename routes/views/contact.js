var keystone = require('keystone');
var Enquiry = keystone.list('Enquiry');

exports = module.exports = function(req, res) {

		var newEnquiry = new Enquiry.model(),
			updater = newEnquiry.getUpdateHandler(req);

		updater.process(req.body, {
			flashErrors : true,
			fields: 'message',
			errorMessage: 'There was a problem submitting your enquiry:'
		}, function(err) {
			if (err) {
				var error = {};
				error[err.errors.message.path] = err.errors.message.message;
				res.status(422).json(error);
			} else {
				res.json({success : true, message : 'Done, thanks'});
			}
		});


};
