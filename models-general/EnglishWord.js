/**
 * Created by User pc on 5/23/2016.
 */


var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var schema = new Schema({
	ID_Word: String,
	Word_First_Letter: String,
	Word: String,
	Meaning: String,
	Unit: String,
	Sentence_1: String,
	Conjunction_Type: String,
	Meaning_Arabic: String,
	Conjunction_Type_Arabic: String,
	createdAt: {type: Date, default: Date.now}
});


schema.methods.exportObject = function() {
	return {
		word: this.Word,
		synonym: this.Synonyms,
		unit: this.Unit,
		streamStopped:this.streamStopped,
		sentence:this.Sentence_1,
		meaning:this.Meaning
	};
};


schema.statics.addNewWord = function(ID_Word,Word_First_Letter,Word,Meaning,Unit,Sentence_1,Conjunction_Type,Meaning_Arabic,Conjunction_Type_Arabic,callback) {

	var doc = new this({
		ID_Word: ID_Word,
		Word_First_Letter: Word_First_Letter,
		Word: Word,
		Meaning: Meaning,
		Unit: Unit,
		Sentence_1: Sentence_1,
		Conjunction_Type: Conjunction_Type,
		Meaning_Arabic: Meaning_Arabic,
		Conjunction_Type_Arabic: Conjunction_Type_Arabic
	});
	doc.save(callback);
};


schema.statics.getWordsList = function(lettersArray,unitsArray,callback) {
	this.find({$and:[{Word_First_Letter: { $in : lettersArray}},{Unit: { $in : unitsArray}}]},{},function (err, words) {
		if (err) {
			err = new Error('DB error.');
			err.status = 401;
			console.error(err);
		}
		callback(err,words);
	});
};


module.exports = mongoose.model('EnglishWord', schema);
