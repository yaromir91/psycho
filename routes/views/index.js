var keystone = require('keystone');
var _ = require('underscore');
var async = require('async');

exports = module.exports = function(req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'home';
	locals.page = {};
	locals.form = {};
	locals.slides = [];
	locals.teachers = {
		data : [],
		count : 0
	};
	locals.settingsTable = {
		style: {
			'class' : 'table-homepage'
		},
		data: []
	};
	locals.competitor = {};

	view.on('init', function (next) {


		keystone
			.list('HomePage')
			.model
			.findOne()
			.populate('block5.slides', '-_id avatar title description')
			.populate('block7.UsersSucceeded')
			.populate('block4.competitor')
			.exec(function(err, results) {
				// all result
				locals.page = results;

				var Slider = keystone.list('Slider'),
					Users = keystone.list('UsersSucceeded'),
					Table = keystone.list('Tables'),
					TableColumns = keystone.list('TableColumns'),
					EnquiryPage = keystone.list('EnquiryPage');

				//Get all slides for home page
				locals.slides = results.block5.slides;

				// Get all teachers for home page
				locals.teachers.data = results.block7.UsersSucceeded;
				locals.teachers.count = results.block7.UsersSucceeded.length;

				EnquiryPage.model.findOne().exec(function(err, form) {
					locals.form = form;
				});

				Table.model.populate(results.block4.competitor, {path: 'tableColumns', select : 'titleName -_id'}).then(function (columns) {
					if(!columns) next();
					_.each(columns.tableColumns, function (columnItem) {
						locals.settingsTable.data.push(columnItem.titleName);
					});
				});

				next();
		});

	});


	// Render the view
	view.render('index');

};
