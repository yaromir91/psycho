var keystone = require('keystone');
var Types = keystone.Field.Types;
var _ = require('underscore');

/**
 * Home Page Model
 * ==========
 */

var HomePage = new keystone.List('HomePage', {
	nocreate: true,
	nodelete : true
}),
	RelationshipName = 'Tables';

HomePage.add({
	block1 : {
		mainTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		leftCircleTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		leftCircleDescription : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		rightCircleTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		rightCircleDescription : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true}
	},
	block2 : {
		mainTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		circleTitle1 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		tableForCircle1 : { type: Types.Relationship,  ref: RelationshipName },
		textForCircle1: {type: Types.Html, wysiwyg: true, initial : true, width : 400, },
		videoForCircle1: {type: Types.Url, wysiwyg: true, initial : true, width : 400, },
		circleSubtitle1 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},

		circleTitle2 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		tableForCircle2 : { type: Types.Relationship,  ref: RelationshipName },
		textForCircle2: {type: Types.Html, wysiwyg: true, initial : true, width : 400, },
		videoForCircle2: {type: Types.Url, wysiwyg: true, initial : true, width : 400, },
		circleSubtitle2 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},

		circleTitle3 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		tableForCircle3 : { type: Types.Relationship,  ref: RelationshipName },
		textForCircle3: {type: Types.Html, wysiwyg: true, initial : true, width : 400, },
		videoForCircle3: {type: Types.Url, wysiwyg: true, initial : true, width : 400, },

		circleTitle4 : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		tableForCircle4 : { type: Types.Relationship,  ref: RelationshipName },
		textForCircle4: {type: Types.Html, wysiwyg: true, initial : true, width : 400, },
		videoForCircle4: {type: Types.Url, wysiwyg: true, initial : true, width : 400, },
	},
	block3 : {
		mainTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		subTitle : {
			authorName : { type: Types.TextArray, default: [], initial: true, collapse: true },
			authorDescription : { type: Types.TextArray, default: [], initial: true, collapse: true }
		},
	},
	block4 : {
		mainTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		competitor : { type: Types.Relationship,  ref: 'Tables' }
	},
	block5 : {
		mainTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		slides : { type: Types.Relationship,  ref: 'Slider', many: true},
	},
	block6 : {
		mainTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		description : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
	},
	block7 : {
		mainTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		UsersSucceeded : { type: Types.Relationship,  ref: 'UsersSucceeded', many: true},
	},
	// block8 : {type: String},
	block9 : {
		mainTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		description : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true }
	}

});

HomePage.schema.pre('save', function(next) {

	if((_.isEmpty(this.block2.tableForCircle1)) == (_.isEmpty(this.block2.textForCircle1)) == (_.isEmpty(this.block2.videoForCircle1))){

		next(new Error('Use only one type for Block2: textForCircle1 or tableForCircle1 or videoForCircle1'))
	}


	if((_.isEmpty(this.block2.tableForCircle2)) == (_.isEmpty(this.block2.textForCircle2)) == (_.isEmpty(this.block2.videoForCircle2))){

		next(new Error('Use only one type for Block2: tableForCircle2 or textForCircle2 or videoForCircle2'))
	}

	if((_.isEmpty(this.block2.tableForCircle3)) == (_.isEmpty(this.block2.textForCircle3)) == (_.isEmpty(this.block2.videoForCircle3))){

		next(new Error('Use only one type for Block2: tableForCircle3 or textForCircle3 or videoForCircle3'))
	}


	if((_.isEmpty(this.block2.tableForCircle4)) == (_.isEmpty(this.block2.textForCircle4)) == (_.isEmpty(this.block2.videoForCircle4))){

		next(new Error('Use only one type for Block2: tableForCircle4 or textForCircle4 or videoForCircle4'))
	}
	next();
})



/**
 * Registration
 */

HomePage.register();
