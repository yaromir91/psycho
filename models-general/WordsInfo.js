/**
 * Created by User pc on 6/2/2016.
 */


var config = require('../config');

var mongoose = require('mongoose');

var Schema = mongoose.Schema;


var schema = new Schema({
	language: String,
	num_of_words: Number,
	createdAt: {type: Date, default: Date.now},
	updateAt: {type: Date, default: Date.now}
});
schema.index({ language: 1}, { unique: true });

schema.methods.exportObject = function() {
	return {
		language: this.language,
		num_of_words: this.num_of_words,
		last_update_date: this.updateAt
	};
};



schema.statics.updateDataWithCounter = function(language,newWordsCount,callback) {
	var doc = new this({
		language: language,
		num_of_words: newWordsCount,
		updateAt: new Date()
	});
	this.findOne({language:language},{},function (err, document) {
		if(!document){
			doc.save(callback);
			return;
		}
		document.num_of_words = document.num_of_words + newWordsCount;
		document.updateAt = new Date();
		document.save(callback);
	});
};


schema.statics.getWordsInfo = function(callback) {
	this.find({},{},function (err, languages) {
		if (err) {
			err = new Error('DB error.');
			err.status = 401;
			console.error(err);
		}
		callback(err,languages);
	});
};


module.exports = mongoose.model('WordsInfo', schema);
