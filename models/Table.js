var keystone = require('keystone'),
	Types = keystone.Field.Types;

/**
 * Tables Model
 * ==========
 */

var Tables = new keystone.List('Tables', {
	map : { name : 'titleName'}
});



Tables.add({
	titleName: { type: String , index: true},
	tableColumns: { type: Types.Relationship,  ref: 'TableColumns', many: true, initial: true },
	tableOptions: { type: Types.Relationship,  ref: 'TableOptions', many: true, initial: true }
});


/**
 * Registration
 */

Tables.defaultColumns = 'titleName';
Tables.register();
