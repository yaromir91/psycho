var keystone = require('keystone'),
	_ = require('underscore'),
	CoursesType = keystone.list('CoursesTypes'),
	Courses = keystone.list('Courses');

exports = module.exports = function (req, res) {
	var result = {},
		courseItem = {};
	CoursesType.paginate({
		page: req.query.page || 1,
		perPage: 10,
		maxPages: 10
	})
		.exec(function (err, ct) {

			if(err) res.status(422).json(err);

			if(!ct.results.length) res.json(result);
			
			_.each(ct.results, function (item) {
				courseItem.title = item.title;
				Courses.model
					.find()
					.where('_id')
					.in(item.courses)
					.select('-_id title score description phone email')
					.exec(function (err, courses) {
						courseItem.courses = courses;
						result.courseType = [];
						result.courseType.push(courseItem);
						
						res.json(result);
					});
	
			});

			
	});

	
};
