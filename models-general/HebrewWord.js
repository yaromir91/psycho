/**
 * Created by User pc on 5/23/2016.
 */


var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var schema = new Schema({
	ID: String,
	Unit: String,
	Word_First_Letter: String,
	Word: String,
	Word_With_Nikud: String,
	Meaning: String,
	Synonyms: String,
	Sentence_1: String,
	createdAt: {type: Date, default: Date.now}
});

schema.methods.exportObject = function() {
	return {
		word: this.Word,
		synonym: this.Synonyms,
		unit: this.Unit,
		streamStopped:this.streamStopped,
		nikud:this.Word_With_Nikud,
		sentence:this.Sentence_1,
		meaning:this.Meaning
	};
};



schema.statics.addNewWord = function(ID,Unit,Word_First_Letter,Word,Word_With_Nikud,Meaning,Synonyms,Sentence_1,callback) {

	var doc = new this({
		ID: ID,
		Unit: Unit,
		Word_First_Letter: Word_First_Letter,
		Word: Word,
		Word_With_Nikud: Word_With_Nikud,
		Meaning: Meaning,
		Synonyms: Synonyms,
		Sentence_1: Sentence_1
	});
	doc.save(callback);
};


schema.statics.getWordsList = function(lettersArray,unitsArray,callback) {
	this.find({$and:[{Word_First_Letter: { $in : lettersArray}},{Unit: { $in : unitsArray}}]},{},function (err, words) {
		if (err) {
			err = new Error('DB error.');
			err.status = 401;
			console.error(err);
		}
		callback(err,words);
	});
};

module.exports = mongoose.model('HebrewWord', schema);
