var keystone = require('keystone');
var Types = keystone.Field.Types;
var upload = require('./Slider').paths;

/**
 * User Model
 * ==========
 */

var UsersSucceeded = new keystone.List('UsersSucceeded');

UsersSucceeded.add({
	name: { type: String, required: true, index: true },
	avatar: {
		type: Types.LocalFile, 
		dest: upload.path, 
		prefix : upload.prefix, 
		label: 'Upload Avatar',
		format : function (item, file) {
			return '<img src="' + upload.prefix + file.filename +'" style="max-width: 50px;">';
		}
	},
});



/**
 * Registration
 */

UsersSucceeded.defaultColumns = 'name';
UsersSucceeded.register();
