var keystone = require('keystone');
var Types = keystone.Field.Types;


/**
 * Sources Page Model
 * ==========
 */

var Courses = new keystone.List('Courses', {
	autokey: { path: 'slug', from: 'title', unique: true },
	map : { name: 'slug'}
});


Courses.add({
	slug : { type: String, readonly: true},
	title : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
	score : { type: Types.Money, initial : true, required: true },
	description : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
	phone : { type: Types.Number, initial : true, required: true },
	email : { type: Types.Email, initial : true, required: true }
});



/**
 * Registration
 */

Courses.register();
