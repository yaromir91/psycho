(function (w) {
	'use strict';

	w.app = {
		binds : function () {
			this.formSubmit();
			this.initSlider();
			this.initJCF();
			this.clickDisable();
			this.slideTable();
			this.toggleColumnTable();
			this.moreDetails();
			this.accordionInit();
			this.showTableOnSourcePage();
			this.getTeachers();
			this.readMore();
			this.homePageGetTable();
			this.triggerElements();

			this.coursePageGetVideo();
			this.addProductsToCard();
			this.removeProduct();
			this.initCountProductToCart();
			this.finalOrder();
			
			//init count cart
			app.helper.setCountCart();
		},
		formSubmit : function () {
			$('.submit-form').on({
				submit : function (e) {
					e.preventDefault();
					var form = e.target;
					$.ajax({
						url : form.action,
						type : form.method,
						data : new FormData(form),
						processData: false,
						contentType: false
					}).done(function (res) {
						$(form).parents('.input-box').html(res.message);
					}).fail(function (err) {
						if(typeof err.responseJSON != 'undefined' ){
							var inputName = Object.keys(err.responseJSON)[0];
							var elem = $(form)
								.find('input[name="' + inputName + '"]')
								.on({
									focus : function (e) {
										$(e.target).next('.help-block').remove();
									}
								});
							if(!elem.next().is('.help-block')){
								elem.after('<span class="help-block" style="color: #FF0000;">' + err.responseJSON[inputName] +'</span>')
							}
						}
					});
				}
			})
		},
		initSlider : function () {
			$('.owl-carousel').owlCarousel({
				loop: true,
				margin: 10,
				nav: true,
				navText: false,
				responsive: {
					0: {
						items: 1
					},
				}
			})
		},
		initJCF : function () {
			jcf.replaceAll();
		},
		clickDisable : function () {
			$('.click-disable').off('click');
		},
		slideTable : function(){
			var $slideTable = $('.slide-table');
			if($slideTable.length){
				$slideTable.get(0).addEventListener('click', function(e){
					e.preventDefault();
					if(e.detail > 1) return false;
					$(e.target).parent().find('.tab-box').slideToggle();
				});
			}
		},
		toggleColumnTable : function () {
			$('.manage-columns').on({
				click : function (e) {
					var table = $(e.target).parents('.holder').find('table'),
						index = $(e.target).data('index') + 1;
					$(table).find('tr > th:nth-child(' + index + '), tr > td:nth-child(' + index + ')').toggle();
				}
			});
		},
		moreDetails : function(){
			$('.read-more').on(
				{
					click : function(e) {
						e.preventDefault();
						var $this = $(this);
						var $collapse = $this.closest('.collapse-group').find('.collapse');
						$collapse.collapse('toggle');
				}
			});
		},
		accordionInit : function () {
			var showAccordion = function(elem) {
				elem.toggleClass('active').next("div").slideUp(500);
			}, hideAccordion = function (elem) {
				$(".accordion-box .accord-content").slideUp(500);
				$(".accordion-box .accord-header").removeClass('active');
				elem.toggleClass('active').next("div").slideToggle(500);
			};


			$(".accordion-box .accord-header").click(function() {
				$(this).next("div").is(":visible") ? showAccordion($(this)) : hideAccordion($(this));
			});

		},
		showTableOnSourcePage: function () {
			$('.circular-box').click(function(){
				if($(this).next('.text-box').is(':visible')) {
					$(this).siblings('.text-box').click();
				}else {
					$(this).siblings('.tab-text-box').click();
				}
			});
			$('.reason-section-third .text-box, .reason-section-four .text-box, .reason-section-five .text-box').on('click', function(event){ //TODO visible table at home page
				event.preventDefault();
				var tableClass = app.helper.findTable(event),
					tableId = app.helper.findTableId(event),
					circleText = app.helper.findText(event),
					circleVideo = app.helper.findVideo(event),
					self = this,
					showTable = function () {
						$('.tab-text-box').hide();
						$('.text-box').show();
						$(self).parent().find('.tab-text-box').show();
						$(self).hide();
						$('.tab-box.' + tableClass).slideDown();
					};

				var $tableList = $('.table-list');
				var $table = $(this).parent();
				var $span = $table.parent();

				$('.tab-box').each(function (i, e) {
					if($(e).is(':visible')){
						$(e).hide();
					}
				});
				if(tableId && !$tableList.find('.' + tableClass).length){
					app.helper.getTableInfo({
						tableId: tableId,
						tableClass: tableClass
					}, function (err, res) {
						if(err){
							console.error(err);
						} else {
							if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
								$tableList.insertAfter($span).append(res);
							} else {
								$tableList.append(res);
								showTable();
							}
							showTable();
						}
					});
				} else {
					if((circleText != '') && !$tableList.find('.' + tableClass).length) {
						var $text = "<div class='tab-box " + tableClass + " '><div class='body-tab'>" + circleText + "</div>";

						if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
							$tableList.insertAfter($span).append($text);
							showTable();
						} else {
							$tableList.append($text);
							showTable();
						}

					} else if((circleVideo != '') && !$tableList.find('.' + tableClass).length) {
						var $video = "<div class='tab-box " + tableClass + " '><iframe src=" + circleVideo + "/></div>";

						if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
							$tableList.insertAfter($span).append($video);
							showTable();
						} else {
							$tableList.append($video);
							showTable();
						}

					} else {
						if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
							$tableList.insertAfter($span);
							showTable();
						}
						showTable();
					}
				}
			});


			showTabs('.reason-section-third');
			showTabs('.reason-section-four');
			showTabs('.reason-section-five');

			function showTabs(className) {
				$( className +' .tab-text-box').on('click', function(event){
					event.preventDefault();
					var elem = $(this),
						tableClass = app.helper.findTable(event);

					$(this).parents(className).find('.tab-box.' + tableClass).slideUp();

					function hideElem(elem) {
						elem.hide();
					}
					setTimeout(function() {//key up
						hideElem(elem);
						elem.parent().find('.text-box').show();
						//$('.table-list').empty();//clear table
					}, 360);
				});
			}


		},
		getTeachers: function () {
			$('teachers').on({
				click: function (e) {
					e.preventDefault();
					var $self = $(e.target),
						teachersWrapper = $(e.target).parents('.holder'),
						teachersIds = [];
						$(teachersWrapper).find('div.col-md-2').filter(function (i,e) {
							teachersIds.push($(e).data('teacher-id'));
						});

					$.ajax({
						url : '/teachers',
						type : 'POST',
						data : {
							teachers : teachersIds
						}
					}).done(function (res) {
						$(teachersWrapper).find('.join-box .row').append(res);
						$self.remove();
					}).fail(function (err) {
						$self.remove();
					});

				}
			})
		},
		readMore: function () {
			$('.read-more').on({
				click: function (e) {
					e.preventDefault();

					var $textWrap = $(e.target).siblings('.text'),
						text = $textWrap.html();
					if($textWrap.hasClass('read-more-ellipsis')) {
						$textWrap.removeClass('read-more-ellipsis');
						$textWrap.text('');
						$textWrap.append('<div class="wrapper-text" style="display: none;">' + text + '</div>');
						$('.wrapper-text').slideDown();
						$(e.target).hide();
					} else {
						$textWrap.slideUp('slow', function () {
							$textWrap.addClass('read-more-ellipsis');
							$textWrap.html($('.wrapper-text').html());
							$textWrap.show();
						});
					}
				}
			});

			$('.why-us-section .more').on({
				click: function (e) {
					e.preventDefault();
					var $text = $('.read-more-description-ellipsis');


					if($text.css('height') >= '38px'){
						$text.attr('data-height', $text.css('height'));
						$text.animate({height: $text.prop('scrollHeight')}, function () {
							$(e.target).hide();
						});
					} else {
						$text.animate({height: $text.attr('data-height')});
					}


				}
			})
		},
		helper: {
			findTable : function (elem) {
				return $(elem.target).parents('[data-table-id]').data('table-id');
			},
			findTableId : function (elem) {
				return $(elem.target).parents('[data-table]').data('table');
			},
			findText: function(elem) {
				return $(elem.target).parents('[data-table]').data('text');
			},
			findVideo: function(elem) {
				return $(elem.target).parents('[data-table]').data('video');
			},
			getTableInfo : function (data, cb) {
				if(!$.isEmptyObject(data)){
					$.ajax({
						url : '/table',
						type : 'POST',
						data : data
					}).done(function (res) {
						cb(null, res)
					}).fail(function (err) {
						cb(err, null)
					});
				} else {
					console.error('data is empty');
				}
			},
			setCountCart : function () {
				var products = JSON.parse(app.helper.Cookies.getItem('products')),
					count = 0;
				if(!products) return;
				
				for(var obj in products){
					count += +products[obj].count
				}
				$('.cart-count').text(count ? count : '');				
			}
			,
			Cookies : {
			    getItem: function (sKey) {
					if (!sKey) { return null; }
					return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
			    },
			    setItem: function (sKey, sValue, vEnd, sPath, sDomain, bSecure) {
					if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return false; }
					var sExpires = "";
					if (vEnd) {
						switch (vEnd.constructor) {
						case Number:
							sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd;
							break;
						case String:
							sExpires = "; expires=" + vEnd;
							break;
						case Date:
							sExpires = "; expires=" + vEnd.toUTCString();
							break;
						}
					}
					document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
					return true;
			    },
			    removeItem: function (sKey, sPath, sDomain) {
					if (!this.hasItem(sKey)) { return false; }
					document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "");
					return true;
			    },
				hasItem: function (sKey) {
					if (!sKey) { return false; }
					return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
				},
			    keys: function () {
					var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
						for (var nLen = aKeys.length, nIdx = 0; nIdx < nLen; nIdx++) { aKeys[nIdx] = decodeURIComponent(aKeys[nIdx]); }
					return aKeys;
			    }
			}
		},
		homePageGetTable: function () {
			var $elem = $('.get_competitors');
			if($elem.length){
				$elem.get(0).addEventListener('click', function(e){
					e.preventDefault();
					if(e.detail > 1) return false;

					if($(e.target).next('.tab-box').length) return;
					app.helper.getTableInfo({
						tableId: app.helper.findTableId(e),
						tableClass: 'table-homepage'

					}, function (err, res) {
						$(e.target).after(res);
						$(e.target).siblings('.tab-box').slideDown();
					})
				});
			}
		},

		coursePageGetVideo: function() {
			var $button = $('.fancybox');
			if($button.data('href') != '') {
				$button.on({
				click: function(e) {
					e.preventDefault();
					var url = $(e.target).data('href');
						$.fancybox({
							autoSize	: false,
							closeClick	: false,
							openEffect	: 'none',
							closeEffect	: 'none',
							type        : 'iframe',
							href		: url,
							helpers: {
								overlay: {
									locked: false
								}
							}
						})
					}
				});

			}

		},

		addProductsToCard: function() {
			$('.add-cart').on({
			click: function(e) {
					e.preventDefault();

					var currentElem = $(this),
						currentPosition = currentElem.offset(),
						cartPosition = $('[data-cart]').offset();

				var cloneCurrent = currentElem.clone();
				currentElem.parents('.col-sm-4').css('position', 'static');
				$(this).after(cloneCurrent);
					cloneCurrent.css({
						position: 'absolute',
						left: currentPosition.left ,
						top : currentPosition.top 
					}).animate({
						left: cartPosition.left - 20,
						top : cartPosition.top ,
						opacity : '0'
					}, 1000, 
					function () {
						this.remove();
						app.helper.setCountCart();
					});
					var prod = {},
						prod_id = $(e.target).data('id');
                    
					var $elm = $(this).parents('.content-box, .circular-box');
					prod.title = $elm.find('div.title').text();
					prod.text = $elm.find('div.text').text();
					prod.price = $elm.find('span.prices').text();
					prod.options = $elm.find('span.option').text();
					prod.discount = $elm.find('span.line-through-text').text();
					prod.image = $elm.parent().find('.image img').attr('src');
					prod.count = 1;
					var c = JSON.parse(app.helper.Cookies.getItem('products'));
					if(c != null){
						if(c[prod_id] != undefined){
							c[prod_id].count = c[prod_id].count+1;
						} else {
							c[prod_id] = prod;
						}
					}else{
						c = {};
						c[prod_id] = prod;
					}
					
					for(var obj in c) {
						c[obj].totalPrice = c[obj].count * c[obj].price
					}
					app.helper.Cookies.setItem('products', JSON.stringify(c), 3600, '/');

					if($(this).data('location') === 'card') {
						location.href = "/card";
					}

				}
			});
		},
		removeProduct: function () {
			var self = this;
			$('.delete-product').on({
				click: function (e) {
					e.preventDefault();
					var products = JSON.parse(self.helper.Cookies.getItem('products'));
					if(confirm('הסר מוצר?')){
						var product_id = $(this).parents('[data-id]').data('id');
						delete products[product_id];
						app.helper.Cookies.setItem('products', JSON.stringify(products));
						app.helper.setCountCart();
						$(this).parents('[data-id]').remove();
					}
					if(Object.keys(products).length == 0){
						app.helper.setCountCart();
						$('.card-wrapper').html('עגלת הקניות ריקה');
					}
				}
			});

		},
		initCountProductToCart: function () {
			var self = this;

			$('.counter-product-plus').on({
				click: function (e) {
					e.preventDefault();
					var inputCount = $(this).siblings('.counter-product'),
						product_id = $(this).parents('[data-id]').data('id');

					var res = inputCount.val(+inputCount.val() + 1);
					var input = $(this).parents('.counter-box').find('input');
					var inputValue = $(this).parents('.counter-box').find('input').attr('value');
					var finalPrice = 0;

					inputValue = input.val();

					var priceOfProduct = $(this).parents('[data-id]').find('td.countPrice').text();
					var priceOfProduct1 = $(this).parents('[data-id]').find('td.price');
					var Price = (priceOfProduct) * inputValue;
					var delivery = $('.delivery').text() * 1;
					var countPrice = Price.toString();
					var totalPrice = $('.custom-table-card-summary').find('span.price');
					
					priceOfProduct1.text(countPrice.toString());
					$('.holder').find('td.price').each(function(i, e) {
						finalPrice += ($(this).text()) * 1;
					});
					var spanPrice = $('.custom-table-card-summary').find('td').last().find('span');

						spanPrice.text(finalPrice.toString());
					var products = JSON.parse(self.helper.Cookies.getItem('products')),
						currentProduct = products[product_id];
					currentProduct.count = inputValue;
					app.helper.setCountCart(Object.keys(products).length);
						
					for(var obj in products) {
						products[obj].totalPrice = products[obj].count * products[obj].price
					}
					
					if(!$('[name^=delivery]').prop('checked')) {
						totalPrice.text((totalPrice.text() * 1) + delivery);
					}
					app.helper.Cookies.setItem('products', JSON.stringify(products), 3600, '/');
					
					app.helper.setCountCart();
				}
			});
			$('.counter-product-minus').on({
				click: function (e) {
					e.preventDefault();
						var inputCount = $(this).siblings('.counter-product'),
						product_id = $(this).parents('[data-id]').data('id');

					if(inputCount.val() <= 1) return;
					var res = inputCount.val(+inputCount.val() - 1);
					var input = $(this).parents('.counter-box').find('input');
					var inputValue = $(this).parents('.counter-box').find('input').attr('value');
					var productPrice = $(this).parents('.bottom-border').find('td.price').text();
					var finalPrice = 0;
					var delivery = $('.delivery').text() * 1;
					inputValue = input.val();

					var priceOfProduct = $(this).parents('[data-id]').find('td.countPrice').text();
					var priceOfProduct1 = $(this).parents('[data-id]').find('td.price');
					var totalPrice = $('.custom-table-card-summary').find('span.price');
					var Price = priceOfProduct1[0].innerText - priceOfProduct;
					var countPrice = Price.toString();
					priceOfProduct1.text(countPrice.toString());

					$('.holder').find('td.price').each(function(i, e) {
						finalPrice += $(this).text() * 1;
					});
					var spanPrice = $('.custom-table-card-summary').find('td').last().find('span');
						spanPrice.text(finalPrice.toString());
					var products = JSON.parse(self.helper.Cookies.getItem('products')),
						currentProduct = products[product_id];
						currentProduct.count = inputValue;
					
					for(var obj in products) {
						products[obj].totalPrice = products[obj].count * products[obj].price
					}
					
					if(!$('[name^=delivery]').prop('checked')) {
						totalPrice.text((totalPrice.text() * 1) + delivery);
					}
					
					app.helper.Cookies.setItem('products', JSON.stringify(products), 3600, '/');
					app.helper.setCountCart();
				}
			});

			$('.counter-product').on('change', function(e) {
				var input = $(this).val();
				var totalPrice = $('.custom-table-card-summary').find('span.price');
				var products = $('td.price');
				var priceOfProduct = $(this).parents('[data-id]').find('td.countPrice');
				var priceOfProduct1 = $(this).parents('[data-id]').find('td.price');
				var delivery = $('.delivery').text() * 1;
				
				
				if(input < 1) {
					return $(this).val(1);
				} else {
					var inputPrice = input * priceOfProduct.text();
					priceOfProduct1.text(inputPrice.toString());
				}
				
				var sum = 0;
				products.each(function(index, elm) {
					sum += $(elm).text() * 1;
				});
				if(!$('[name^=delivery]').prop('checked')) {
					totalPrice.text(sum + delivery);
				}else {
					totalPrice.text(sum);
				}				
			}).on({
				focus : function () {
					$(this).select();
				}
			});

			$('[name^=delivery]').on('change',function (e){
				var delivery = $('.delivery').text() * 1;
				var totalPrice = $('.custom-table-card-summary').find('span.price');
				
				$('[name^=delivery]').prop('checked', false);
				this.checked = true;
				jcf.refreshAll();
				if(!$('[name^=delivery]').prop('checked')) {
					totalPrice.text((totalPrice.text() * 1) + delivery);
				}else {
					totalPrice.text((totalPrice.text() * 1) - delivery);
				}
			});


			(function calculatePriceByDelivery() {
				var cookies = app.helper.Cookies.getItem('products');
				var delivery = $('.delivery').text() * 1;
				var price = $('td.price');
				var priceTd = $('td.countPrice');
				var inputValue = $('.counter-product');
				var totalPrice = $('.custom-table-card-summary').find('span.price');
				var json = JSON.parse(cookies);
				for(var obj in json) {
					json[obj].totalPrice = json[obj].count * json[obj].price
				}
				app.helper.Cookies.setItem('products', JSON.stringify(json), 3600, '/');
				if(!$('[name^=delivery]').prop('checked')) {
					totalPrice.text((totalPrice.text() * 1) + delivery);
				}
				
			})()
		},
		finalOrder: function() {
			var self = this;
			$('.get-order').on({
				click: function(e) {
					e.preventDefault();
					var delivery = $('.delivery').text() * 1;
					var button = $(this).parents('.btn-card').find('button.get-order').text();
					var products = JSON.parse(self.helper.Cookies.getItem('products'));
					var order = [];
					for(var obj in products) {
						order.push({id: obj, count: products[obj].count});
					}
					if(!$('[name^=delivery]').prop('checked')) {
						order.push({delivery: delivery})
					}

					app.helper.Cookies.setItem('order', JSON.stringify(order));
					var orderFromCookie = app.helper.Cookies.getItem('order');
					$.ajax({
						url: '/order',
						method: 'post',
						data: orderFromCookie,
						success: function(res) {
							//location.href = '/order';
							//app.helper.Cookies.removeItem('products');
						}
					})
				}
			});
		},
		triggerElements: function () {
			$('.icon-green').on({
				click : function () {
					var $elem = $(this).siblings('.number').find('a');
					if($elem.length){
						$elem.get(0).click();
					}

				}
			})
		}
	};

}(window = window || {}));
