var keystone = require('keystone');
var Types = keystone.Field.Types;

var ProductSchema = new keystone.List('ProductSchema', {
	map : { name : 'schemaName'}
}),
RelationshipName = 'Product';

ProductSchema.add({
	schemaName: { type: String, wysiwyg: true, initial : true, width : 400, required: true, readonly: true},
	title: { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true },
	product: { type: Types.Relationship,  ref: RelationshipName, many: true, required: true, initial : true	},
	color: { type: Types.Color, wysiwyg: true, initial : true },
	block1: {
		display: { type: Types.Boolean, initial : true, },
		rightSideBarTitle: { type: Types.TextArray, default: [], initial: true,  },
		rightSideBarDescription: { type: Types.TextArray, default: [], initial: true,  },
		leftSideBarTitle: { type: Types.TextArray, default: [], initial: true, },
		leftSideBarDescription: { type: Types.TextArray, default: [], initial: true, }
	},
	block2: {
		display: { type: Types.Boolean, initial : true, },
		title: { type: Types.Html, wysiwyg: true, initial : true, width : 400  },
		description: { type: Types.Html, wysiwyg: true, initial : true, width : 400 }
	},
	block3: {
		display: { type: Types.Boolean, initial : true },
		mainTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400 },
		title: { type: Types.Html, wysiwyg: true, initial : true, width : 400 },
		description: { type: Types.Html, wysiwyg: true, initial : true, width : 400 }
	}

});


ProductSchema.register();
