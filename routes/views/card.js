var keystone = require('keystone'),
	_ = require('underscore'),
	Card = keystone.list('Card');

exports = module.exports = function(req, res) {
	var view = new keystone.View(req, res);
	var locals = res.locals;
	locals.productsFromCookie = req.cookies.products ? JSON.parse(req.cookies.products) : [];
	locals.products = [];
	locals.delivery = false;
	locals.priceOfAllProducts = 0;
	locals.card = {};
	view.on('init', function(toRender) {
		_.each(locals.productsFromCookie, function(item, id) {
			//var prod = {};
			item.id = id;
			locals.products.push(item);
			locals.priceOfAllProducts = locals.priceOfAllProducts + (item.price * item.count);
		});
		Card.model.findOne().exec(function(err, card) {
			locals.card = card;
		});
		console.log(locals.productsFromCookie);
		toRender();
	});

	view.render('card');

}
