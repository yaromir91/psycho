var keystone = require('keystone');
var Types = keystone.Field.Types;
var upload = require('./Slider').paths;

/**
 * Product Model
 * ==========
 */

var Product = new keystone.List('Product', {
	map : { name : 'name'}
});

Product.add({
	name: { type: String , required: true, index: true, initial: true},
	rating : { type: Types.Money, required : true ,initial: true },
	prices : {
		total : { type : Types.Money, required : true, initial : true, index : true, label : 'Price'},
		totalDiscount : { type : Types.Money, required : false, initial : true, index : true},
		discount : { type : Types.Boolean, initial : true}
	},
	createdAt : { type: Date , default: Date.now },
	status : { type : Types.Boolean, initial : true},
	description : { type: Types.Html, wysiwyg: true, initial : true, width : 400},
	option: { type: Types.Html, wysiwyg: true, initial : true, width : 400 },
	image: { type: Types.LocalFile,
		dest: upload.path,
		prefix : upload.prefix,
		label: 'Upload Avatar',
		format : function (item, file) {
			return '<img src="' + upload.prefix + file.filename +'" style="max-width: 50px;">';
		}
	},
	phone: {type: Types.Number, initial : true, required: true}
});


// Custom product validate
// Product.schema.pre('validate', function (next) {
// 	if(!/^([1-5])$/i.test(this.rating)){
// 		next(new Error('Rating not valid'));
// 	}
// 	next();
// });

/**
 * Registration
 */

Product.defaultColumns = 'name, createdAt, status';
Product.register();
