var keystone = require('keystone');
var Types = keystone.Field.Types;


var WordsPage = new keystone.List('WordsPage', {
		nocreate: true,
		nodelete : true
	}),
	RelationshipName = 'Tables';


WordsPage.add({

	block4 : {
		mainTitle : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
		UsersSucceeded : { type: Types.Relationship,  ref: 'UsersSucceeded', many: true},
	},


});



/**
 * Registration
 */

WordsPage.register();
