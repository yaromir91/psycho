var keystone = require('keystone'),
	async = require('async'),
	_ = require('underscore');

exports = module.exports = function(req, res) {


	var view = new keystone.View(req, res);
	var locals = res.locals;

	locals.page = {};
	locals.classNameSlider = 'slider-section-second';
	locals.slides = [];
	locals.form = {};
	locals.teachers = {
		data: [],
		count: 0
	};



	view.on('init', function (toRender) {


		keystone
			.list('CoursesPage')
			.model
			.findOne()
			.exec(function(err, results) {
				// all result

				locals.page = results;

				var Slider = keystone.list('Slider'),
					Users = keystone.list('UsersSucceeded'),
					Table = keystone.list('Tables'),
					CoursesPage = keystone.list('CoursesPage'),
					TableColumns = keystone.list('TableColumns'),
					EnquiryPage = keystone.list('EnquiryPage');

					EnquiryPage.model.findOne().exec(function(err, form) {
						locals.form = form;
					})

				//Get all slides for home page
				async.each(locals.page.block3.slides, function(slideId, next) {

					Slider.model.findById(slideId).exec(function(err, slide) {

						locals.slides.push(slide);

						next(err);
					});

				}, function(err) {
					toRender(err);
				});

				// Get all teachers for home page
				var teachers = locals.page.block4.UsersSucceeded;
				Users.model.find().where('_id').in(teachers).limit(6).exec(function(err, users) {
					locals.teachers.data = users;
					locals.teachers.count = teachers.length;
				});
				//console.log(locals.page);
				Table.model.findById(locals.page.block4.competitor).exec(function (err, table) {
					if(table){
						TableColumns.model.find().where('_id').in(table.tableColumns).exec(function (err, columns) {
							_.each(columns, function (columnItem) {
								locals.settingsTable.data.push(columnItem.titleName);
							});
						});
					}

				});

			});
	});


	view.render('courses');

};
