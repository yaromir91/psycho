var keystone = require('keystone'),
	_ = require('underscore'),
	ProductSchema = keystone.list('ProductSchema'),
	SourcesPage = keystone.list('SourcesPage'),
	Product = keystone.list('Product');

exports = module.exports = function(req, res) {
		var view = new keystone.View(req, res);
		var locals = res.locals;

		locals.schema = {};
		locals.productsId = [];
		locals.products = [];
		locals.title = '';
		locals.description = '';
		locals.rightSideBarTitle = [];
		locals.rightSideBarDescription = [];
		locals.leftSideBarTitle = [];
		locals.leftSideBarDescription = [];

		SourcesPage.model.findOne().exec(function(err, sourcesPage) {
			var icon = req.params.icon;
			if(_.isNull(icon)) return res.status(404).render('errors/404');

			var schemaId = sourcesPage.block3['productForIcon' + icon].schema,
				productIds = sourcesPage.block3['productForIcon' + icon].items;


			if(_.isEmpty(productIds)){
				/**
				 * Get schema products
				 */
				ProductSchema.model.findOne().where('_id', schemaId).exec(function(err, schema) {
					locals.schema = schema;
					_.each(schema.product, function(item) {
						locals.productsId.push(item)
					});

					if(schema.block1.display === true) {
						_.each(schema.block1.rightSideBarTitle, function(item) {
							locals.rightSideBarTitle.push(item)
						});
						_.each(schema.block1.rightSideBarDescription, function(item) {
							locals.rightSideBarDescription.push(item)
						});
						_.each(schema.block1.leftSideBarTitle, function(item) {
							locals.leftSideBarTitle.push(item)
						});
						_.each(schema.block1.leftSideBarDescription, function(item) {
							locals.leftSideBarDescription.push(item)
						})
					}

					if(schema.block2.display === true) {
						locals.title = schema.block2.title;
						locals.description = schema.block2.description;
					}

					if(schema.block3.display === true) {
						locals.mainTitle = schema.block3.mainTitle;
						locals.title = schema.block3.title;
						locals.description = schema.block3.description;
					}

					Product.model.find().where('_id').in(locals.productsId).exec(function(err, products) {
						_.each(products, function(prod) {
							locals.products.push(prod);
						})
					});

					view.render('productSchema');

				});

			} else {
				/**
				 * Get all products for current circle
				 */
				Product.model.find()
					.where('status', true)
					.where('_id')
					.in(productIds)
					.exec(function (err, products) {
						locals.products = products;

						view.render('products');
					});

			}

		});

};
