var keystone = require('keystone');

/**
 * TableOptions Model
 * ==========
 */

var TableOptions = new keystone.List('TableOptions', {
	map : { name : 'titleName'}
});

TableOptions.add({
	titleName: { type: String , index: true},
});

/**
 * Registration
 */

TableOptions.defaultColumns = 'titleName';
TableOptions.register();
