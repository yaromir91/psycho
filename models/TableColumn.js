var keystone = require('keystone'),
	Types = keystone.Field.Types;

/**
 * TableColumns Model
 * ==========
 */

var TableColumns = new keystone.List('TableColumns', {
	map : { name : 'titleName'}
});

TableColumns.add({
	titleName: { type: String , required: true},
	optionValue: { type: Types.TextArray, default: [], initial: true, collapse: true }
});

/**
 * Registration
 */

TableColumns.defaultColumns = 'titleName';
TableColumns.register();
