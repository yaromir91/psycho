var keystone = require('keystone');
var Types = keystone.Field.Types;


/**
 * Sources Page Model
 * ==========
 */

var CoursesInfo = new keystone.List('CoursesInfo', {
	label : 'Course Info',
	autokey: { path: 'title', from: 'titleOne', unique: true },
	map : { name: 'title'}
});


CoursesInfo.add({
	title : { type: String, readonly: true },
	titleOne : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
	descriptionOne : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
	titleTwo : { type: Types.Html, wysiwyg: true, initial : true, width : 400, required: true},
	bullets : {  type: Types.TextArray, default: [], initial: true, collapse: true },
	videoUrl : {  type: Types.Url, initial: true, required: true},
	phone : {  type: Types.Number, initial: true, required: true},
	email : {  type: Types.Email, initial: true, required: true},
	testimonials : {  type: Types.Relationship, ref: 'Testimonials', many: true}
});



/**
 * Registration
 */

CoursesInfo.register();
