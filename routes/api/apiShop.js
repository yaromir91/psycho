var keystone = require('keystone');

exports = module.exports = function(req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;
	
	locals.page = {};
	
	
	view.on('init', function (toRender) {
		
		
		keystone
			.list('SourcesPage')
			.model
			.findOne()
			.exec(function(err, results) {
				// all result
				locals.page = results;
				toRender(err);
			});
	});
	
	view.render('apiShop');

};
