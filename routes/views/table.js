var keystone = require('keystone'),
	async = require('async'),
	_ = require('underscore');
var Tables = keystone.list('Tables'),
	TableOptions = keystone.list('TableOptions'),
	TableColumns = keystone.list('TableColumns');

exports = module.exports = function(req, res) {
		var view = new keystone.View(req, res);
		var locals = res.locals;

		locals.name = req.body.tableClass;
		locals.table = {};
		
		view.on('init', function (toRender) {	
			var tableId = req.body.tableId;

			Tables
				.model
				.findById(tableId)
				.exec(function (err, table) {
					if(table){
						var result = {};
						var getOptions = function (callback) {
							TableOptions.model.find().where('_id').in(table.tableOptions).exec(function (err, options) {
								result.options = [];
								_.each(options, function (optionItem) {
									result.options.push(optionItem.titleName);
								});
								callback(null, result);
							});
						};

						var getColumn = function (callback) {
							TableColumns.model.find().where('_id').in(table.tableColumns).exec(function (err, columns) {
								result.columns = [];
								_.each(columns, function (columnItem) {
									result.columns.push(columnItem);
								});
								callback(null, result);
							});
						};

						async.parallel([getOptions, getColumn], function (err) {
							locals.table = result;
							toRender(err);
						});

					} else {
						toRender(err);
					}
					
				});
			
			
		});
	
		view.render('../partials/table');
};
