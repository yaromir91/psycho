var keystone = require('keystone');
var _ = require('underscore');
var Types = keystone.Field.Types;

/**
 * Static Pages Model
 * ==========
 */

var StaticPage = new keystone.List('StaticPage', {
	nocreate: false,
	noedit: false,
	map : { name : 'title'},
	autokey: { path: 'url', from: 'title'},
});

StaticPage.add({
	url : { type: String, noedit: true },
	title: { type: String, initial: true, required: true },
	menu: {
		name: {type: String, initial: true, required: true, unique: true },
		position: { type: Types.Number, initial : true, unique: true }
	},
	htmlContent: { type: Types.Html, wysiwyg: true, initial: true, width: 400, height: 600 },
	metaTitle: { type: String },
	metaDescription: { type: String },
	metaKeyWords: { type: String },
	display: { type: Boolean, initial: true }
});

StaticPage.schema.pre('save', function (next) {
	if(_.isEmpty(this.metaTitle)){
		this.metaTitle = this.title;
	}
	next();
});

/**
 * Registration
 */

StaticPage.defaultColumns = 'navigationName';
StaticPage.register();
