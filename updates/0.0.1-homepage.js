var keystone = require('keystone'),
	Types = keystone.Field.Types,
	async = require('async'),
	_ = require('underscore'),
	HomePageModel = keystone.list('HomePage'),
	Tables = keystone.list('Tables'),
	TableOptions = keystone.list('TableOptions'),
	TableColumns = keystone.list('TableColumns')
	;

var tableOptions = [{
	"titleName": "םירפס"
}, {
	"titleName": "ימצע דומילל ז״ול"
}, {
	"titleName": "תויצלומיס"
}, {
	"titleName": "םירוביח תקידב"
}, {
	"titleName": "םירועיש"
}, {
	"titleName": "?ךלש ךירדמה ימ"
}, {
	"titleName": "לוגרת תונכות"
}, {
	"titleName": "ריחמ"
}
];


var tableColumns = [
	{
		"titleName": "וכיספ",
		"optionValue": [
			"םירפס 20  תולאש 10,394\t",
			"םימי יפל תטרופמ\t",
			"6",
			"1-6",
			"ןיילנוא םירועיש",
			",ןויסינ תונש 15 - רציווש דעלא, םינחבמו םירפס רבחמ םיכירדמ רישכמ\t",
			"םינחבמ ,םילימ תנכות תויצלומיס חותינו6\t", "ח״ש 1,199-1,699\t"]
	}, {
		"titleName": "SelfClass ויק-ייה",
		"optionValue": [
			"םירפס 20  תולאש 10,394\t",
			"םימי יפל תטרופמ\t",
			"6",
			"1-6",
			"ןיילנוא םירועיש",
			",ןויסינ תונש 15 - רציווש דעלא, םינחבמו םירפס רבחמ םיכירדמ רישכמ\t",
			"םינחבמ ,םילימ תנכות תויצלומיס חותינו6\t", "ח״ש 1,199-1,699\t"]
	}, {
		"titleName": "Home םודיק",
		"optionValue": [
			"םירפס 20  תולאש 10,394\t",
			"םימי יפל תטרופמ\t",
			"6",
			"1-6",
			"ןיילנוא םירועיש",
			",ןויסינ תונש 15 - רציווש דעלא, םינחבמו םירפס רבחמ םיכירדמ רישכמ\t",
			"םינחבמ ,םילימ תנכות תויצלומיס חותינו6\t", "ח״ש 1,199-1,699\t"]
	}, {
		"titleName": "חוור בינ",
		"optionValue": [
			"םירפס 20  תולאש 10,394\t",
			"םימי יפל תטרופמ\t",
			"6",
			"1-6",
			"ןיילנוא םירועיש",
			",ןויסינ תונש 15 - רציווש דעלא, םינחבמו םירפס רבחמ םיכירדמ רישכמ\t",
			"םינחבמ ,םילימ תנכות תויצלומיס חותינו6\t", "ח״ש 1,199-1,699\t"]
	}
];

var table = {
	"titleName" : "home page migration",
	"tableOptions": [],
	"tableColumns": []
};


var homePage = {
	block1 : {
		mainTitle : '<b>פסיכו</b> - ההכנה הטובה ביותר לפסיכומטרי',
		leftCircleTitle : 'ספרים <br> ולימוד עצמי',
		leftCircleDescription : '| אפליקציה, תרגול אונליין, ספר 700 ועוד |',
		rightCircleTitle : 'קורס <br> פסיכומטרי',
		rightCircleDescription : '| בוא ללמוד עם מדריך צמוד בקצב שלך |'
	},
	block2 : {
		mainTitle : 'למה ללמוד דווקא איתנו?',
		circleTitle1 : 'יותר תרגול <br> בלה בלה בלה',
		circleSubtitle1 : 'הייפצל ץחל',
		textForCircle1 : 'הייפצל ץחל',
		
		circleTitle2 : 'יותר תרגול <br> בלה בלה בלה',
		circleSubtitle2 : 'הייפצל ץחל',
		textForCircle2 : 'הייפצל ץחל',
		
		circleTitle3 : 'יותר תרגול <br> בלה בלה בלה',
		circleSubtitle3 : 'הייפצל ץחל',
		textForCircle3 : 'הייפצל ץחל',
		
		circleTitle4 : 'יותר תרגול <br> בלה בלה בלה',
		circleSubtitle4 : 'הייפצל ץחל',
		textForCircle4 : 'הייפצל ץחל'
	},
	block3 : {
		mainTitle : 'למי מתאים ללמוד לבד?',
		subTitle : {
			authorName : ['תלמידים חזקים'],
			authorDescription : ['בלה בלה בלה בלה בלה בלה בלה בלה בלה בלה בלה בלהה'],
		},
	},
	block4 : {
		mainTitle : 'אנחנו מול המתחרים',
		competitor : 'a76sd786as78d67as68'

	},
	block5 : {
		mainTitle : 'מקצוענים ממליצים',
	},
	block6 : {
		mainTitle : 'למה לבחור בספרים שלנו?',
		description : '<b>| מדריכי פסיכומטרי, מנהלי מסיכומטרי ומורים ותיקים למתמטיקה</b> מכל הארץ מלמדים עם הספרים שלנו. <br> אלה אנשים מקצוע שבאמת מבינים פסיכומטרי ולא מושפעים מנציגי מכירות או מפרסומות.' +
		'<b>| מדריכי פסיכומטרי, מנהלי מסיכומטרי ומורים ותיקים למתמטיקה</b> מכל הארץ מלמדים עם הספרים שלנו. <br> אלה אנשים מקצוע שבאמת מבינים פסיכומטרי ולא מושפעים מנציגי מכירות או מפרסומות.' +
		'<b>| מדריכי פסיכומטרי, מנהלי מסיכומטרי ומורים ותיקים למתמטיקה</b> מכל הארץ מלמדים עם הספרים שלנו. <br> אלה אנשים מקצוע שבאמת מבינים פסיכומטרי ולא מושפעים מנציגי מכירות או מפרסומות.'
	},
	block7 : {
		mainTitle : 'הצטרפו לאלפים שכבר הצליחו',
		UsersSucceeded : []
	},
	block8 : {
		mainTitle : 'כמה תקבל בפסיכומטרי?',
		leftIconTitle : 'שלחו הודעה',
		rightIconTitle : 'התקשרו אלינו, אנחנו ממש נחמדים בטלפון :-)',
		phone : '0773010950'
	},
	block9 : {
		mainTitle : '| מה עדיף - ללמוד לבד או בקורס?',
		description : '<div class="blue-line"></div> ' +
					'<h3 class="title">| מה עדיף - ללמוד לבד או בקורס?</h3> ' +
					'<div class="sub-title"> נתחיל בהמלצה של המרכז הארצי לבחינות והערכה: </div> ' +
					'<div class="strong-text"> <b> ״המסגרת המתאימה להכנה היא עיניין של סגנון אישי: יש הזקוקים למסגרת נוקשה ויש המעדיפים לימוד עצמי. מכל מקום, לפני שתבחרו במסגרת' +
					' כלשהי, רצוי שתבדקו היטב אם היא מספקת לכם תירגול יסודי. אל תסתמכו על שמועות לא מבוססות בדבר ' +'שיפורים עצומים שהשיגו תלמידי קורס כלשהו בציוניהם וגם לא על נוסחאות קסם - אין כאלה!״</b> </div> ' +
					'<div class="text"> כן - אין תשובה חד משמעית האם עדיף ללמוד לבד לקראת מבחן פסיכומרי בעזרת קורס פסיכומטרי או בלימוד עצמי, זה באמת עיניין אישי. ככלל,' +
					' ככל שתלמיד יותר חזק עדיף לו ללמוד לבד. <span class="color-blue"><b>תלמיד חזק שילמד לבד יקבל ציון גבוה יותר מאשר אם ילמד בקורס!</b></span> נכו' +
					'ן, זה נשמע קצת מוזר, אבל הסיבה לכך היא שבקורס הזמן שלו מתבזבז על שאלות של תלמידים אחרים במקום להתמקד בצרכים שלו. </div>'
	},

};

function createTables(done) {
	async.series([
			function (next) {

				_.each(tableOptions, function (item) {
					var newTableOptions = new TableOptions.model(item);
					newTableOptions.save(function (err, data) {
						if (err) {
							console.error("Error  to the database:");
							console.error(err, '<<<<', item);
						} else {
							table.tableOptions.push(data._id);
						}
					});
				});
				next();
			},
			function (next) {

				_.each(tableColumns, function (item) {
					var newTableColumns = new TableColumns.model(item);
					newTableColumns.save(function (err, data) {
						if (err) {
							console.error("Error  to the database:");
							console.error(err);
						} else {
							table.tableColumns.push(data._id);
						}
					});
				});

				next();
			},
			function (next) {
				var newTableOptions = new Tables.model(table);
				newTableOptions.save(function (err, data) {
					if (err) {
						console.error("Error  to the database:");
						console.error(err);
						next(err);
					} else {
						homePage.block4.competitor = data._id;
						console.log('Added table in source page.');
						next();
					}
				});
			}
		],
		function () {
			done()
		})

}
	
function createHomePage(done){
	var newHomePage = new HomePageModel.model(homePage);
	
	newHomePage.save(function(err) {
		if (err) {
			console.error("Error  to the database:");
			console.error(err);
		} else {
			console.log("Added to the database.");
		}
		done(err);
	});
}

exports = module.exports = function(done) {
	async.series([
			createTables,
			createHomePage
		],
		function (e, res) {
			done()
		}

	);};
