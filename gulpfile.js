var gulp = require('gulp');
var jshint = require('gulp-jshint');
var jshintReporter = require('jshint-stylish');
var watch = require('gulp-watch');
var shell = require('gulp-shell');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var minifyCSS = require('gulp-minify-css');
var bs = require('browser-sync').create();


var paths = {
	'src':['./models/**/*.js', './routes/**/*.js', 'keystone.js', 'package.json'],
	'templates' : ['./templates/**/*.swig'],
	'sass' : ['./public/styles/main.scss']
};

// gulp lint
gulp.task('lint', function(){
	gulp.src(paths.src)
		.pipe(jshint())
		.pipe(jshint.reporter(jshintReporter));
});

gulp.task('sass', function () {
	return gulp.src(paths.sass)
		.pipe(sass())
		.pipe(minifyCSS(''))
		.pipe(rename('main.min.css'))
		.pipe(gulp.dest('./public/styles'));
});

// gulp watcher for lint
gulp.task('watch:lint', function () {
	gulp.watch(paths.src, ['lint']).on('change', bs.reload);
	gulp.watch(paths.templates).on('change', bs.reload);
	gulp.watch(paths.sass, ['sass']).on('change', bs.reload);
});

gulp.task('server:dev', shell.task('nodemon keystone.js --ignore public/js/'));
gulp.task('connect:dev', function() {
	bs.init({
		proxy : 'localhost',
		port : 81,
		open : false
	});
});


gulp.task('runKeystone', shell.task('node keystone.js'));
gulp.task('runKeystoneDev', shell.task('nodemon keystone.js --ignore public/js/'));
gulp.task('watch', [
  'watch:lint'
]);

gulp.task('default', ['watch', 'runKeystone']);
gulp.task('build', ['lint', 'sass']);
gulp.task('dev', ['watch', 'runKeystoneDev'/*, 'connect:dev'*/]);
