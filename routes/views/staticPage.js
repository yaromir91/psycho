var keystone = require('keystone'),
	_ = require('underscore');

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res);
	var locals = res.locals;
	var SP = keystone.list('StaticPage');
	
	view.on('init', function (next) {

		SP.model.findOne()
			.where('url', req.params.staticPage)
			.where('display', true)
			.exec(function (err, sp) {
				if(_.isNull(sp)) return res.status(404).render('errors/404');
				
				locals.section = sp.url;
				locals.content = sp.htmlContent;
				locals.meta = {
					title : sp.metaTitle,
					description : sp.metaDescription,
					keyWords : sp.metaKeyWords
				};
				next();
			});

		
	});

	view.render('static-page');
	
};
